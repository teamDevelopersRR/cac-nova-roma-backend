-- COURSE
INSERT INTO course VALUES (1, SYSDATE(), SYSDATE(), 'COMP', 'CIÊNCIA DA COMPUTAÇÃO', 60, NULL, NULL);
INSERT INTO course VALUES (2, SYSDATE(), SYSDATE(), 'DIR', 'DIREITO', 60, NULL, NULL);
INSERT INTO course VALUES (3, SYSDATE(), SYSDATE(), 'CONT', 'CIÊNCIAS CONTÁBEIS', 60, NULL, NULL);
INSERT INTO course VALUES (4, SYSDATE(), SYSDATE(), 'ENG_PROD', 'ENGENHARIA DE PRODUÇÃO', 60, NULL, NULL);
INSERT INTO course VALUES (5, SYSDATE(), SYSDATE(), 'ENG_CIVIL', 'ENGENHARIA CIVIL', 60, NULL, NULL);
INSERT INTO course VALUES (6, SYSDATE(), SYSDATE(), 'ADM', 'ADMINISTRAÇÃO', 60, NULL, NULL);

-- PROFILE TYPE
INSERT INTO profile_type VALUES(1, SYSDATE(), SYSDATE(), 'ADMIN', '', 'ADMIN', NULL, NULL);
INSERT INTO profile_type VALUES(2, SYSDATE(), SYSDATE(), 'SECRETARY', '', 'SECRETARIA', NULL, NULL);
INSERT INTO profile_type VALUES(3, SYSDATE(), SYSDATE(), 'STUDENT', '', 'ESTUDANTE', NULL, NULL);

-- PROFILE
INSERT INTO profile VALUES (1, SYSDATE(), SYSDATE(), 1, 'ADMIN', NULL, NULL, 1);
INSERT INTO profile VALUES (2, SYSDATE(), SYSDATE(), 2, 'SECRETARIA', NULL, NULL, 2);
INSERT INTO profile VALUES (3, SYSDATE(), SYSDATE(), 2, 'ALUNO', NULL, NULL, 3);

-- USER
INSERT INTO users VALUES (1, SYSDATE(), SYSDATE(), '06221683475', 'rubensFerreira', 'Rubens Ferreira', 'd12452a2b62fd983a7738701eba96ddfcf979e65e26689a2b20262985faddd61', NULL, NULL, 1, 1);
INSERT INTO users VALUES (2, SYSDATE(), SYSDATE(), '07570077407', 'pauloRenato', 'Renatão Alves', '94d79f5c02b515acf35d89f8fadb47944f8a09c1089f6442cf9b32ea5a4516c3', 1, 1, 1, 1);

-- SENHA eduardo.calabria --
INSERT INTO users VALUES (3, SYSDATE(), SYSDATE(), '00000000000', 'eduardo.calabria', 'Eduardo Calábria', '2512f811c814183e925fba89684b416060250b6423005b4ab44c9b22d70416cc', 2, 2, 2, 2);

-- SENHA cbpe@1.. --
INSERT INTO users VALUES (4, SYSDATE(), SYSDATE(), '01910000000', '201810001', 'Mathew Murdok', '9665022841b879f74d0de3b0495aa88a7ae1c12993a209b789ee2b1159584984', 2, 2, 1, 3);
INSERT INTO users VALUES (5, SYSDATE(), SYSDATE(), '00191000000', '201810002', 'Jéssica Jones', '9665022841b879f74d0de3b0495aa88a7ae1c12993a209b789ee2b1159584984', 2, 2, 1, 3);

INSERT INTO users VALUES (6, SYSDATE(), SYSDATE(), '19100000000', '201410324', 'Rubens Ferreira', 'd12452a2b62fd983a7738701eba96ddfcf979e65e26689a2b20262985faddd61', 2, 2, 1, 3);

-- DISCIPLINE
INSERT INTO discipline VALUES (1, SYSDATE(), SYSDATE(), 'ATIVIDADE COMPLEMENTAR 1', 72, 1, 2);
INSERT INTO discipline VALUES (2, SYSDATE(), SYSDATE(), 'ATIVIDADE COMPLEMENTAR 2', 72, 1, 2);

-- PERMISSION
INSERT INTO permission VALUES (1, SYSDATE(), SYSDATE(), 1, 'ROLE_USER', 1, 1);
INSERT INTO permission VALUES (2, SYSDATE(), SYSDATE(), 1, 'ROLE_PROFILE', 1, 1);
INSERT INTO permission VALUES (3, SYSDATE(), SYSDATE(), 1, 'ROLE_ACTIVITY', 1, 1);
INSERT INTO permission VALUES (4, SYSDATE(), SYSDATE(), 1, 'ROLE_ADMIN', 1, 1);
INSERT INTO permission VALUES (5, SYSDATE(), SYSDATE(), 1, 'ROLE_SECRETARY', 1, 1);
INSERT INTO permission VALUES (6, SYSDATE(), SYSDATE(), 1, 'ROLE_STUDENT', 1, 1);
INSERT INTO permission VALUES (7, SYSDATE(), SYSDATE(), 1, 'ROLE_ACTIVITY_TYPE', 1, 1);
INSERT INTO permission VALUES (8, SYSDATE(), SYSDATE(), 1, 'ROLE_COURSE', 1, 1);
INSERT INTO permission VALUES (9, SYSDATE(), SYSDATE(), 1, 'ROLE_DISCIPLINE', 1, 1);
INSERT INTO permission VALUES (10, SYSDATE(), SYSDATE(), 1, 'ROLE_ACTIVITY_TYPE_COURSE', 1, 1);
INSERT INTO permission VALUES (11, SYSDATE(), SYSDATE(), 1, 'ROLE_COURSE_DISCIPLINE', 1, 1);
INSERT INTO permission VALUES (12, SYSDATE(), SYSDATE(), 1, 'ROLE_PERMISSION', 1, 1);
INSERT INTO permission VALUES (13, SYSDATE(), SYSDATE(), 1, 'ROLE_PROFILE_PERMISSION', 1, 1);
INSERT INTO permission VALUES (14, SYSDATE(), SYSDATE(), 1, 'ROLE_PROFILE_TYPE', 1, 1);

-- PROFILE PERMISSION
INSERT INTO profile_permission VALUES (1, SYSDATE(), SYSDATE(), 1, 1, 1, 1, 1, 1, 1, 1);
INSERT INTO profile_permission VALUES (2, SYSDATE(), SYSDATE(), 1, 1, 1, 1, 1, 1, 2, 1);
INSERT INTO profile_permission VALUES (3, SYSDATE(), SYSDATE(), 1, 1, 1, 1, 1, 1, 3, 1);
INSERT INTO profile_permission VALUES (4, SYSDATE(), SYSDATE(), 1, 1, 1, 1, 1, 1, 4, 1);
INSERT INTO profile_permission VALUES (5, SYSDATE(), SYSDATE(), 1, 1, 1, 1, 1, 1, 5, 1);
INSERT INTO profile_permission VALUES (6, SYSDATE(), SYSDATE(), 1, 1, 1, 1, 1, 1, 6, 1);
INSERT INTO profile_permission VALUES (7, SYSDATE(), SYSDATE(), 1, 1, 1, 1, 1, 1, 7, 1);
INSERT INTO profile_permission VALUES (8, SYSDATE(), SYSDATE(), 1, 1, 1, 1, 1, 1, 8, 1);
INSERT INTO profile_permission VALUES (9, SYSDATE(), SYSDATE(), 1, 1, 1, 1, 1, 1, 9, 1);
INSERT INTO profile_permission VALUES (10, SYSDATE(), SYSDATE(), 1, 1, 1, 1, 1, 1, 10, 1);
INSERT INTO profile_permission VALUES (11, SYSDATE(), SYSDATE(), 1, 1, 1, 1, 1, 1, 11, 1);
INSERT INTO profile_permission VALUES (12, SYSDATE(), SYSDATE(), 1, 1, 1, 1, 1, 1, 12, 1);
INSERT INTO profile_permission VALUES (13, SYSDATE(), SYSDATE(), 1, 1, 1, 1, 1, 1, 13, 1);
INSERT INTO profile_permission VALUES (14, SYSDATE(), SYSDATE(), 1, 1, 1, 1, 1, 1, 14, 1);

INSERT INTO profile_permission VALUES (15, SYSDATE(), SYSDATE(), 0, 1, 1, 1, 1, 1, 1, 2);
INSERT INTO profile_permission VALUES (16, SYSDATE(), SYSDATE(), 0, 1, 1, 1, 1, 1, 2, 2);
INSERT INTO profile_permission VALUES (17, SYSDATE(), SYSDATE(), 0, 1, 1, 1, 1, 1, 3, 2);
INSERT INTO profile_permission VALUES (18, SYSDATE(), SYSDATE(), 0, 1, 1, 1, 1, 1, 4, 2);
INSERT INTO profile_permission VALUES (19, SYSDATE(), SYSDATE(), 0, 1, 1, 1, 1, 1, 5, 2);
INSERT INTO profile_permission VALUES (20, SYSDATE(), SYSDATE(), 0, 1, 1, 1, 1, 1, 6, 2);
INSERT INTO profile_permission VALUES (21, SYSDATE(), SYSDATE(), 0, 1, 1, 1, 1, 1, 7, 2);
INSERT INTO profile_permission VALUES (22, SYSDATE(), SYSDATE(), 0, 1, 1, 1, 1, 1, 8, 2);
INSERT INTO profile_permission VALUES (23, SYSDATE(), SYSDATE(), 0, 1, 1, 1, 1, 1, 9, 2);
INSERT INTO profile_permission VALUES (24, SYSDATE(), SYSDATE(), 0, 1, 1, 1, 1, 1, 10, 2);
INSERT INTO profile_permission VALUES (25, SYSDATE(), SYSDATE(), 0, 1, 1, 1, 1, 1, 11, 2);
INSERT INTO profile_permission VALUES (26, SYSDATE(), SYSDATE(), 0, 1, 1, 1, 1, 1, 12, 2);
INSERT INTO profile_permission VALUES (27, SYSDATE(), SYSDATE(), 0, 1, 1, 1, 1, 1, 13, 2);
INSERT INTO profile_permission VALUES (28, SYSDATE(), SYSDATE(), 0, 1, 1, 1, 1, 1, 14, 2);

INSERT INTO profile_permission VALUES (29, SYSDATE(), SYSDATE(), 1, 1, 1, 1, 1, 1, 3, 3);
INSERT INTO profile_permission VALUES (30, SYSDATE(), SYSDATE(), 0, 1, 1, 1, 1, 1, 6, 3);
INSERT INTO profile_permission VALUES (31, SYSDATE(), SYSDATE(), 0, 0, 1, 0, 1, 1, 7, 3);

-- ACTIVITY TYPE
INSERT INTO activity_type VALUES(1, SYSDATE(), SYSDATE(), 'OCCURRENCE', 'Descrição 1', 'PALESTRA', 1, 2);
INSERT INTO activity_type VALUES(2, SYSDATE(), SYSDATE(), 'HOUR', 'Descrição 2','CURSO EXTRA CURRICULAR', 1, 2);
INSERT INTO activity_type VALUES(3, SYSDATE(), SYSDATE(), 'HOUR', 'Descrição 3', 'MONITORIA', 1, 2);
INSERT INTO activity_type VALUES(4, SYSDATE(), SYSDATE(), 'OCCURRENCE', 'Descrição 4', 'REPRESENTANTE DE TURMA', 1, 2);

-- COURSE DISCIPLINE
INSERT INTO course_discipline VALUES(1, SYSDATE(), SYSDATE(), 1, 2, 1, 1);
INSERT INTO course_discipline VALUES(2, SYSDATE(), SYSDATE(), 1, 2, 1, 2);
INSERT INTO course_discipline VALUES(3, SYSDATE(), SYSDATE(), 1, 2, 2, 1);
INSERT INTO course_discipline VALUES(4, SYSDATE(), SYSDATE(), 1, 2, 2, 2);
INSERT INTO course_discipline VALUES(5, SYSDATE(), SYSDATE(), 1, 2, 3, 1);
INSERT INTO course_discipline VALUES(6, SYSDATE(), SYSDATE(), 1, 2, 3, 2);
INSERT INTO course_discipline VALUES(7, SYSDATE(), SYSDATE(), 1, 2, 4, 1);
INSERT INTO course_discipline VALUES(8, SYSDATE(), SYSDATE(), 1, 2, 4, 2);
INSERT INTO course_discipline VALUES(9, SYSDATE(), SYSDATE(), 1, 2, 5, 1);
INSERT INTO course_discipline VALUES(10, SYSDATE(), SYSDATE(), 1, 2, 5, 2);
INSERT INTO course_discipline VALUES(11, SYSDATE(), SYSDATE(), 1, 2, 6, 1);
INSERT INTO course_discipline VALUES(12, SYSDATE(), SYSDATE(), 1, 2, 6, 2);

-- ACTIVITY TYPE COURSE
INSERT INTO activity_type_course VALUES (1, SYSDATE(), SYSDATE(), NULL, 36, 1, NULL, '2018.2', 1, 1, 2, 1);
INSERT INTO activity_type_course VALUES (2, SYSDATE(), SYSDATE(), NULL, 36, 1, NULL, '2018.2', 1, 1, 3, 1);
INSERT INTO activity_type_course VALUES (3, SYSDATE(), SYSDATE(), 3, NULL, NULL, 15, '2018.2', 1, 1, 1, 1);
INSERT INTO activity_type_course VALUES (4, SYSDATE(), SYSDATE(), 10, NULL, NULL, 6, '2018.2', 1, 1, 4, 1);
INSERT INTO activity_type_course VALUES (5, SYSDATE(), SYSDATE(), NULL, 36, 1, NULL, '2018.2', 1, 1, 2, 2);
INSERT INTO activity_type_course VALUES (6, SYSDATE(), SYSDATE(), NULL, 36, 1, NULL, '2018.2', 1, 1, 3, 2);
INSERT INTO activity_type_course VALUES (7, SYSDATE(), SYSDATE(), 3, NULL, NULL, 15, '2018.2', 1, 1, 1, 2);
INSERT INTO activity_type_course VALUES (8, SYSDATE(), SYSDATE(), 10, NULL, NULL, 6, '2018.2', 1, 1, 4, 2);

-- ACTIVITY
INSERT INTO activity VALUES (1, SYSDATE(), SYSDATE(), 1, CURDATE(), 'Curso de Inglês', 15, 15, 0, NULL, NULL, NULL, 'PENDING', 'TOTAL', 4, 4, 2, 4);
INSERT INTO activity_history VALUES (1, SYSDATE(), SYSDATE(), 'CREATED', 'PENDING', 4, 4, 1);
INSERT INTO activity VALUES (2, SYSDATE(), SYSDATE(), 1, CURDATE(), 'Curso de Francês', 28, 5, 23, NULL, NULL, NULL, 'PENDING', 'PARTIAL',	4,	4, 2, 4);
INSERT INTO activity_history VALUES (2, SYSDATE(), SYSDATE(), 'CREATED', 'PENDING', 4, 4, 2);
INSERT INTO activity VALUES (3, SYSDATE(), SYSDATE(), 1, CURDATE(), 'Palestra de Direito Digital', NULL, 6, 0, 2, 2, 0, 'PENDING', 'TOTAL', 4, 4, 1, 4);
INSERT INTO activity_history VALUES (3, SYSDATE(), SYSDATE(), 'CREATED', 'PENDING', 4, 4, 3);
INSERT INTO activity VALUES (4, SYSDATE(), SYSDATE(), 1, CURDATE(), 'Palestra de SCRUM', NULL, 6, 0, 2, 2, 0, 'PENDING', 'TOTAL', 4, 4, 1, 4);
INSERT INTO activity_history VALUES (4, SYSDATE(), SYSDATE(), 'CREATED', 'PENDING', 4, 4, 4);
INSERT INTO activity VALUES (5, SYSDATE(), SYSDATE(), 1, CURDATE(), 'Curso de Libras', 20, 0, 20, NULL, NULL, NULL, 'PENDING', 'GLOSA', 4, 4, 2, 4);
INSERT INTO activity_history VALUES (5, SYSDATE(), SYSDATE(), 'CREATED', 'PENDING', 4, 4, 5);
INSERT INTO activity VALUES (6, SYSDATE(), SYSDATE(), 1, CURDATE(), 'Cálculo de Palestra Android', NULL, 9, 5, 4, 3, 1, 'PENDING', 'PARTIAL', 4, 4, 1, 4);
INSERT INTO activity_history VALUES (6, SYSDATE(), SYSDATE(), 'CREATED', 'PENDING', 4, 4, 6);
INSERT INTO activity VALUES (7, SYSDATE(), SYSDATE(), 1, CURDATE(), 'Palestra Experiência de Ionic v1 e v2', NULL, 0, 10, 2, 0, 2, 'PENDING', 'TOTAL', 4, 4, 1, 4);
INSERT INTO activity_history VALUES (7, SYSDATE(), SYSDATE(), 'CREATED', 'PENDING', 4, 4, 7);

-- Atualizar registros que foram inseridas com essas colunas como nula
UPDATE profile SET id_user_insert = 1, id_user_update = 2 WHERE id_profile != 0;
UPDATE profile_type SET id_user_insert = 1, id_user_update = 2 WHERE id_profile_type != 0;
UPDATE course SET id_user_insert = 1, id_user_update = 2 WHERE id_course != 0;