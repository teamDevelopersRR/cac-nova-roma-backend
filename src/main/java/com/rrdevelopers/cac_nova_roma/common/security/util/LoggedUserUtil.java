/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.common.security.util;

import org.springframework.security.core.context.SecurityContextHolder;

import com.rrdevelopers.cac_nova_roma.common.security.model.LoggedUser;
import com.rrdevelopers.cac_nova_roma.control.model.User;

/**
 * @author rubens.ferreira
 *
 */
public class LoggedUserUtil {
	public static User getUser() {
		LoggedUser loggedUser = (LoggedUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return loggedUser.getUser() == null ? null : loggedUser.getUser();
	}
}
