package com.rrdevelopers.cac_nova_roma.common.security.service;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.rrdevelopers.cac_nova_roma.common.security.manager.TokenManager;
import com.rrdevelopers.cac_nova_roma.common.security.model.LoggedUser;
import com.rrdevelopers.cac_nova_roma.common.security.model.TokenInfo;
import com.rrdevelopers.cac_nova_roma.common.security.model.TokenInfoSystem;
import com.rrdevelopers.cac_nova_roma.common.security.model.TokenInfoUser;

/**
 * @author rubens_ferreira
 *
 */
@Service
public class AuthenticationService {

	@Autowired
	private AuthenticationManager authenticationManager;

	public LoggedUser validateToken(String token) {

		LoggedUser loggedUser = null;

		TokenInfo tokenInfo = null;

		if ("passthrough_token".equals(token)) {
			tokenInfo = new TokenInfoSystem("passthrough_token", "passthrough_ip");
		} else {
			tokenInfo = TokenManager.getTokenInfo(token);
		}

		if (Objects.nonNull(tokenInfo)) {
			Authentication authentication = this.authenticationManager.authenticate(getAuthentication(tokenInfo));
			SecurityContextHolder.getContext().setAuthentication(authentication);
			if (authentication.getPrincipal() != null) {
				loggedUser = (LoggedUser) authentication.getPrincipal();
				loggedUser.setTokenInfo(tokenInfo);
				loggedUser.setToken(token);
			}
		}
		return loggedUser;
	}

	private Authentication getAuthentication(TokenInfo tokenInfo) {
		switch (tokenInfo.getType()) {
		case USER:
			TokenInfoUser tokenInfoUser = (TokenInfoUser) tokenInfo;
			return new UsernamePasswordAuthenticationToken(tokenInfoUser.toPlainToken(), tokenInfoUser.getPassword());
		case SYSTEM:
			TokenInfoSystem tokenInfoSystem = (TokenInfoSystem) tokenInfo;
			return new UsernamePasswordAuthenticationToken(tokenInfoSystem.toPlainToken(), "");

		default:
			return null;
		}
	}

	public LoggedUser autenticate(String login, String password, String ip) {

		LoggedUser result = null;

		if (Objects.nonNull(login)) {
			TokenInfo tokenInfo = new TokenInfoUser(login, password, ip);
			Authentication authentication = new UsernamePasswordAuthenticationToken(getAuthentication(tokenInfo),
					password);
			authentication = this.authenticationManager.authenticate(authentication);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			if (authentication.getPrincipal() != null) {
				result = (LoggedUser) authentication.getPrincipal();
				String token = TokenManager.generateToken(tokenInfo);
				result.setTokenInfo(tokenInfo);
				result.setToken(token);
			}
		}
		return result;
	}

	public LoggedUser register(String mac, String ip) {

		LoggedUser result = null;

		if (Objects.nonNull(mac)) {
			TokenInfo tokenInfo = new TokenInfoSystem(mac, ip);
			Authentication authentication = new UsernamePasswordAuthenticationToken(getAuthentication(tokenInfo), "");
			authentication = this.authenticationManager.authenticate(authentication);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			if (authentication.getPrincipal() != null) {
				result = (LoggedUser) authentication.getPrincipal();
				String token = TokenManager.generateToken(tokenInfo);
				result.setTokenInfo(tokenInfo);
				result.setToken(token);
			}
		}
		return result;
	}

}
