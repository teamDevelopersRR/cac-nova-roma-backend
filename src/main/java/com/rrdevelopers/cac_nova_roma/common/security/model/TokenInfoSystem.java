package com.rrdevelopers.cac_nova_roma.common.security.model;

import java.util.Date;

/**
 * @author rubens_ferreira
 *
 */
public class TokenInfoSystem extends TokenInfo {
	private String mac;

	public TokenInfoSystem() {
	}

	public TokenInfoSystem(String mac, String ip) {
		super(ip, TypeToken.SYSTEM, new Date());
		this.mac = mac;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	@Override
	public String toString() {
		return getField(getType().name()) + getField(this.mac) + this.getDateGenerate().getTime();
	}
}
