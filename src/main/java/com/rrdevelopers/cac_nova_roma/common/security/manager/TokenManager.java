package com.rrdevelopers.cac_nova_roma.common.security.manager;

import java.util.Objects;

import com.rrdevelopers.cac_nova_roma.common.security.exception.InvalidKeyException;
import com.rrdevelopers.cac_nova_roma.common.security.model.TokenInfo;


/**
 * @author rubens_ferreira
 *
 */
public class TokenManager {
	
	public static String generateToken(TokenInfo tokenInfo, String key) throws InvalidKeyException {

		return CryptoManager.encrypt(tokenInfo.toPlainToken(), key);
	}

	public static String generateToken(TokenInfo tokenInfo) {
		String result = null;
		try {
			result = generateToken(tokenInfo, null);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		}

		return result;
	}

	public static boolean validateToken(String token, String key) throws InvalidKeyException {

		return Objects.nonNull(getTokenInfo(token, key));
	}

	public static boolean validateToken(String token) {

		return Objects.nonNull(getTokenInfo(token));
	}

	public static TokenInfo getTokenInfo(String token, String key) throws InvalidKeyException {
		TokenInfo result = null;

		String decriptedToken = CryptoManager.decrypt(token, key);

		if (Objects.nonNull(decriptedToken)) {
			return TokenInfo.getTokenInfoByPlainToken(decriptedToken);
		}

		return result;
	}

	public static TokenInfo getTokenInfo(String token) {
		TokenInfo result = null;
		try {
			result = getTokenInfo(token, null);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		}

		return result;
	}
}
