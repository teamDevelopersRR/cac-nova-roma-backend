package com.rrdevelopers.cac_nova_roma.common.security.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author rubens_ferreira
 *
 */
@ConfigurationProperties("b7.ieadpe")
public class SecurityConfigurationProperties {
	private boolean debug = false;
	private boolean passthrough = false;
	private boolean showException = true;

	public boolean isDebug() {
		return debug;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	public boolean isPassthrough() {
		return passthrough;
	}

	public void setPassthrough(boolean passthrough) {
		this.passthrough = passthrough;
	}

	public boolean isShowException() {
		return showException;
	}

	public void setShowException(boolean exibirExcecao) {
		this.showException = exibirExcecao;
	}

}
