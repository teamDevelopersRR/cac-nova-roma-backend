package com.rrdevelopers.cac_nova_roma.common.security.service;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.rrdevelopers.cac_nova_roma.common.security.model.LoggedUser;
import com.rrdevelopers.cac_nova_roma.common.security.model.TokenInfo;
import com.rrdevelopers.cac_nova_roma.common.security.model.TokenInfoSystem;
import com.rrdevelopers.cac_nova_roma.common.security.model.TokenInfoUser;
import com.rrdevelopers.cac_nova_roma.control.model.User;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IUserService;

/**
 * @author rubens_ferreira
 *
 */
@Service
public class CACUserDetailsService implements UserDetailsService {
	
	@Autowired
	private IUserService userService;

	@Override
	public UserDetails loadUserByUsername(String dataLogin) throws UsernameNotFoundException {

		if (Objects.nonNull(dataLogin)) {
			TokenInfo tokenInfo = TokenInfo.getTokenInfoByPlainToken(dataLogin);

			switch (tokenInfo.getType()) {
			case USER:
				TokenInfoUser tokenInfoUsuario = (TokenInfoUser) tokenInfo;
				User user = this.userService.findByLogin(tokenInfoUsuario.getLogin());
				if (Objects.isNull(user)) {
					throw new UsernameNotFoundException("User " + tokenInfoUsuario.getLogin() + " Not Found.");
				}
				return new LoggedUser(user);

			case SYSTEM:
				TokenInfoSystem tokenInfoSystem = (TokenInfoSystem) tokenInfo;
				return new LoggedUser(tokenInfoSystem.getMac(), tokenInfoSystem);
			default:
				throw new UsernameNotFoundException("Undentified token type");
			}
		} else {
			throw new UsernameNotFoundException("Data not reported");
		}

	}

}
