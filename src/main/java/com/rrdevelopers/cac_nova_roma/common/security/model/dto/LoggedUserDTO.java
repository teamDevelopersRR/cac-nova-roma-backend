package com.rrdevelopers.cac_nova_roma.common.security.model.dto;

import java.util.List;

import com.rrdevelopers.cac_nova_roma.control.model.User;
import com.rrdevelopers.cac_nova_roma.control.model.enums.ProfileCategoryEnum;

/**
 * @author rubens_ferreira
 *
 */
public class LoggedUserDTO {

	private long id;

	private String login;

	private String name;

	private ProfileCategoryEnum typeProfile;

	private List<String> roles;

	public LoggedUserDTO(User user) {
		this.roles = (List<String>) user.getRoles();
		this.id = user.getId();
		this.login = user.getLogin();
		this.name = user.getName();
		this.typeProfile = user.getProfile().getType().getCategory();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getRoles() {
		return roles;
	}

	public ProfileCategoryEnum getTypeProfile() {
		return typeProfile;
	}

	public void setTypeProfile(ProfileCategoryEnum typeProfile) {
		this.typeProfile = typeProfile;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}
}
