package com.rrdevelopers.cac_nova_roma.common.security.filter;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import com.rrdevelopers.cac_nova_roma.common.security.manager.CryptoManager;
import com.rrdevelopers.cac_nova_roma.common.security.model.LoggedUser;
import com.rrdevelopers.cac_nova_roma.common.security.service.AuthenticationService;

/**
 * @author rubens_ferreira
 *
 */
@Component
public class TokenAuthenticationFilter extends GenericFilterBean {

	private static final String HEADER_TOKEN = "x-auth-token";
	private static final String HEADER_IP = "ip";
	private static final String PARAM_LOGIN = "login";
	private static final String PARAM_PASSWORD = "password";
	private static final String PARAM_MAC = "mac";

	private static final String REQUEST_ATTR_DO_NOT_CONTINUE = "TokenAuthenticationFilter-doNotContinue";

	@Value("${rr.cac.passthrough}")
	private Boolean passthrough;

	@Autowired
	private AuthenticationService authenticationService;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		
		String token = httpRequest.getHeader(HEADER_TOKEN);
		String ip = httpRequest.getHeader(HEADER_IP);

		String login = httpRequest.getParameter(PARAM_LOGIN);
		String password = getPasswordHash(httpRequest.getParameter(PARAM_PASSWORD));
		String mac = httpRequest.getParameter(PARAM_MAC);

		String errorMessage = "";
		LoggedUser loggedUser = null;

		System.out.println(httpRequest.getRequestURI());

		try {
			if ((httpRequest.getRequestURI().endsWith("/authentication/user")
					|| httpRequest.getRequestURI().endsWith("/authentication/user/"))
					&& httpRequest.getMethod().equals("POST")) {
				errorMessage = "Invalid user or password.";
				loggedUser = this.authenticationService.autenticate(login, password, ip);
			} else if ((httpRequest.getRequestURI().endsWith("/register")
					|| httpRequest.getRequestURI().endsWith("/register/")) && httpRequest.getMethod().equals("POST")) {
				errorMessage = "Invalid mac.";
				loggedUser = this.authenticationService.register(mac, ip);
			} else {
				errorMessage = "Invalid token.";
				if (passthrough || canPassThrough(httpRequest)) {
					token = "passthrough_token";
				}
				if (Objects.nonNull(token)) {
					loggedUser = this.authenticationService.validateToken(token);
				}
			}
			if (Objects.nonNull(loggedUser)) {
				request.setAttribute("LoggedUser", loggedUser);
				if (Objects.isNull(httpResponse.getHeader(HEADER_TOKEN))) {
					httpResponse.addHeader(HEADER_TOKEN, loggedUser.getToken());
				}
			} else {
				httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Denied access.");
				doNotContinueWithRequestProcessing(httpRequest);
			}
		} catch (BadCredentialsException e) {
			httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, errorMessage);
			doNotContinueWithRequestProcessing(httpRequest);
		}

		if (canRequestProcessingContinue(httpRequest)) {
			chain.doFilter(request, response);
		}

	}

	private String getPasswordHash(String password) {
		try {
			return CryptoManager.toHash256(password);
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

	private boolean canPassThrough(HttpServletRequest request) {
		String url = request.getRequestURL().toString();
		String baseURL = url.substring(0, url.length() - request.getRequestURI().length()) + request.getContextPath()
				+ "/";

		Set<String> urlsMapping = new HashSet<String>();

		// URL's QUE NÃO NECESSITAM DE AUTENTICAÇÃO
		urlsMapping.add(baseURL);
		
		//boolean urlMappingFlag = false;		
		return (request.getRequestURL().toString().equals(baseURL)
				|| request.getRequestURL().toString().equals(baseURL + "congregation/congregationsDTO")
				|| request.getRequestURI().contains("app/") || request.getRequestURI().contains("resources/"));
	}

	private void doNotContinueWithRequestProcessing(HttpServletRequest httpRequest) {
		httpRequest.setAttribute(REQUEST_ATTR_DO_NOT_CONTINUE, "");
	}

	private boolean canRequestProcessingContinue(HttpServletRequest httpRequest) {
		return httpRequest.getAttribute(REQUEST_ATTR_DO_NOT_CONTINUE) == null;
	}

}
