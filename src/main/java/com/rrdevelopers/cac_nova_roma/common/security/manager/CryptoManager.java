package com.rrdevelopers.cac_nova_roma.common.security.manager;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Objects;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author rubens_ferreira
 *
 */
public final class CryptoManager {

	private static final String PREFIX = "#!cac|";
	private static final String _B7 = "_cac############";
	private static final String SUFIX = "!#";
	private static final SimpleDateFormat FORMAT_DATE_KEY = new SimpleDateFormat("yyyyMMdd");
	private static final String ALGORITHM = "AES";
	private static final String HASH_TYPE = "SHA-256";
	private static final String PASSWORD_SALT = "cac_server_Data_security";

	public static void main(String[] args) {
		try {
			System.out.println(toHash256("92673720"));
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	public static String toHash256(String value)throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest algorithm = MessageDigest.getInstance(HASH_TYPE);
        byte messageDigest[] = algorithm.digest((PASSWORD_SALT + value).getBytes("UTF-8"));
         
        StringBuilder hexString = new StringBuilder();
        for (byte b : messageDigest) {
          hexString.append(String.format("%02x", 0xFF & b));
        }
        return hexString.toString();
        
	}
	
	public static String encrypt(String value, String key) {
		String result = null;
		try {
			Cipher cipher = Cipher.getInstance(ALGORITHM);
			cipher.init(Cipher.ENCRYPT_MODE, getKeySpec(key));
			result = new String(Base64.getEncoder().encode(cipher.doFinal(value.getBytes())));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public static String encrypt(String value) {
		String result = null;
		result = encrypt(value, null);

		return result;
	}

	public static String decrypt(String value, String key) {

		String result = null;
		try {
			Cipher cipher = Cipher.getInstance(ALGORITHM);
			cipher.init(Cipher.DECRYPT_MODE, getKeySpec(key));
			result = new String(cipher.doFinal(Base64.getDecoder().decode(value.getBytes())));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public static String decrypt(String value) {
		String result = null;
		result = decrypt(value, null);

		return result;
	}

	private static SecretKeySpec getKeySpec(String key) {
		String salt = PREFIX + FORMAT_DATE_KEY.format(new Date()) + SUFIX;
		if (Objects.nonNull(key)) {
			salt = (key + _B7).substring(0, 16);
		}

		return new SecretKeySpec(salt.getBytes(), ALGORITHM);
	}
}
