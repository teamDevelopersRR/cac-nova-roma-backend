package com.rrdevelopers.cac_nova_roma.common.security.model;

import java.util.Date;

/**
 * @author rubens_ferreira
 *
 */
public class TokenInfoUser extends TokenInfo {
	private String login;

	private String password;

	public TokenInfoUser() {
	}

	public TokenInfoUser(String login, String password, String ip) {
		super(ip, TypeToken.USER, new Date());
		this.login = login;
		this.password = password;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return getField(getType().name()) + getField(this.login) + getField(this.password)
				+ this.getDateGenerate().getTime();
	}
}
