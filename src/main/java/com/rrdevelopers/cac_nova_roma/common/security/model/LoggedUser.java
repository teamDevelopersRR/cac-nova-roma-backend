package com.rrdevelopers.cac_nova_roma.common.security.model;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.rrdevelopers.cac_nova_roma.common.security.model.TokenInfo.TypeToken;
import com.rrdevelopers.cac_nova_roma.control.model.User;

/**
 * @author rubens_ferreira
 *
 */
public class LoggedUser implements UserDetails {
	
	private static final long serialVersionUID = 1L;

	private User user;
	
	private String login;
	
	private String password;
	
	private TokenInfo.TypeToken typeToken;
	
	private String token;
	
	private TokenInfo tokenInfo;
	
	public LoggedUser(User user) {
		this.user = user;
		this.login = user.getLogin();
		this.password = user.getPassword();
		this.typeToken = TypeToken.USER;
	}
	
	public LoggedUser(String login, TokenInfo tokenInfo) {
		this.login = login;
		this.typeToken = tokenInfo.getType();
		this.password = "";
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();

		switch (this.typeToken) {
		case USER:
			authorities.add(new SimpleGrantedAuthority("ROLE_USER_SYSTEM"));
			getUser()
				.getProfile()
				.getRoles()
				.stream()
				.forEach(profile -> authorities.add(new SimpleGrantedAuthority(profile)));
			break;
		case SYSTEM:
			authorities.add(new SimpleGrantedAuthority("ROLE_SYSTEM"));
			break;
		default:
			break;
		}
		return authorities;
	}
	
	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return login;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public User getUser() {
		return user;
	}

	public void setUsuario(User user) {
		this.user = user;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public TokenInfo.TypeToken getTipoToken() {
		return typeToken;
	}

	public void setTipoToken(TokenInfo.TypeToken typeToken) {
		this.typeToken = typeToken;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public TokenInfo getTokenInfo() {
		return tokenInfo;
	}

	public void setTokenInfo(TokenInfo tokenInfo) {
		this.tokenInfo = tokenInfo;
	}

}
