package com.rrdevelopers.cac_nova_roma.common.security.controller;

import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rrdevelopers.cac_nova_roma.common.security.model.LoggedUser;
import com.rrdevelopers.cac_nova_roma.common.security.model.dto.LoggedUserDTO;
import com.rrdevelopers.cac_nova_roma.control.model.User;
import com.rrdevelopers.cac_nova_roma.control.model.UserTrace;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IUserService;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IUserTraceService;

/**
 * @author rubens_ferreira
 *
 */
@RestController
@RequestMapping("/authentication")
public class AuthenticationController {

	@Autowired
	private IUserService userService;

	@Autowired
	private IUserTraceService userTraceService;
	
	@PostMapping("/user")
	public ResponseEntity<LoggedUserDTO> authenticate(@RequestParam("login") String login,
			@RequestParam("password") String password,
			@RequestParam("ip") String ip,
			@RequestParam("action") String action, HttpServletRequest request,
			HttpServletResponse response) {

		LoggedUser loggedUser = (LoggedUser) request.getAttribute("LoggedUser");

		System.out.println("TOKEN -> (" + login + "): " + response.getHeader("x-auth-token"));

		if (Objects.nonNull(loggedUser)) {
			User user = userService.findById(loggedUser.getUser().getId());
			UserTrace userTrace = new UserTrace(action, ip, user);
			
			userTraceService.save(userTrace);
			
			return new ResponseEntity<LoggedUserDTO>(new LoggedUserDTO(loggedUser.getUser()), HttpStatus.OK);
		}
		return null;
	}

}