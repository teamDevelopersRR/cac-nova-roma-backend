/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.common.security.exception;

/**
 * @author rubens.ferreira
 *
 */
public class InvalidKeyException extends Exception {

	private static final long serialVersionUID = 6931956250265525363L;

	public InvalidKeyException(String message) {
		super(message);
	}
}
