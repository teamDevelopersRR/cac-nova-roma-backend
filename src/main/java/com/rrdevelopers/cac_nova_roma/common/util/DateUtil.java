/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.common.util;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author rubens.ferreira
 *
 */
public class DateUtil {

	public static boolean isAfterThanNow(LocalDate date) {
		return date.isAfter(LocalDate.now());
	}
	
	public static boolean isAfterThanNow(LocalDateTime date) {
		return date.isAfter(LocalDateTime.now());
	}

}
