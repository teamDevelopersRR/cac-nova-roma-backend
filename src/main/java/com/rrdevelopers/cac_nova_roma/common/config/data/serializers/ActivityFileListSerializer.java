package com.rrdevelopers.cac_nova_roma.common.config.data.serializers;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.rrdevelopers.cac_nova_roma.control.model.ActivityFile;

public class ActivityFileListSerializer extends StdSerializer<List<ActivityFile>> {

	private static final long serialVersionUID = 1L;

	public ActivityFileListSerializer() {
		this(null);
	}

	protected ActivityFileListSerializer(Class<List<ActivityFile>> t) {
		super(t);
	}

	@Override
	public void serialize(List<ActivityFile> activityFileList, JsonGenerator generator, SerializerProvider provider)
			throws IOException {
		if (Objects.nonNull(activityFileList)) {
			activityFileList.forEach(this::removeFiles);
		}

		generator.writeObject(activityFileList);
	}
	
	private void removeFiles(ActivityFile file) {
		file.setActivity(null);
	}

}
