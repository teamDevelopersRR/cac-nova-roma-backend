package com.rrdevelopers.cac_nova_roma.common.handler.exceptions;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * @author rubens_ferreira
 *
 */
@JsonPropertyOrder({ "status", "error", "message" })
public class AccessException extends BaseException {

	private static final long serialVersionUID = -8770041816606906450L;
	private String error;
	private int status;

	public AccessException(String error, String message, int status) {
		super(null, message);
		this.error = error;
		this.status = status;
		setErrorType(ERROR_TYPE.ACCESS);
	}

	@JsonIgnore
	@Override
	public Throwable getException() {
		return super.getException();
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}