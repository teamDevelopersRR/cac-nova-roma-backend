package com.rrdevelopers.cac_nova_roma.common.hibernate.eventlistener;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Objects;

import org.hibernate.event.spi.PreInsertEvent;
import org.hibernate.event.spi.PreLoadEvent;
import org.hibernate.event.spi.PreUpdateEvent;

import com.rrdevelopers.cac_nova_roma.common.security.manager.CryptoManager;
import com.rrdevelopers.cac_nova_roma.control.model.Authenticated;

/**
 * @author rubens.ferreira 
 */
public class AuthenticatedPreInsertLoadUpdateEventListener extends AbstractPreInsertLoadUpdateListener {

	private static final long serialVersionUID = -4870579007119480794L;

	/**
	 * Responsável por encriptar a senha antes de inserir
	 */
	@Override
	public boolean onPreInsert(PreInsertEvent event) {
		Authenticated authenticated = (Authenticated) event.getEntity();
		List<String> properties = this.listProperties(event);

		String password = authenticated.getPassword();
		if (Objects.nonNull(password)) {
			try {
				password = CryptoManager.toHash256(password);
			} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			if (Objects.nonNull(password)) {
				event.getState()[properties.indexOf(AUTHENTICATED_PASSWORD)] = password;
			}
		}

		return false;
	}

	/**
	 * Responsável por encriptar a senha antes de atualizar
	 */
	@Override
	public boolean onPreUpdate(PreUpdateEvent event) {
		Authenticated authenticated = (Authenticated) event.getEntity();
		List<String> properties = this.listProperties(event);
		
		String oldPassword = (String) event.getOldState()[properties.indexOf(AUTHENTICATED_PASSWORD)];
		String password = authenticated.getPassword();
		
		if (Objects.isNull(password) || "".equals(password.trim())) {
			event.getState()[properties.indexOf(AUTHENTICATED_PASSWORD)] = oldPassword;
			authenticated.setPassword(oldPassword);
		} else if (!oldPassword.equals(password)){
			try {
				password = CryptoManager.toHash256(password);
			} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			if (Objects.nonNull(password)) {
				event.getState()[properties.indexOf(AUTHENTICATED_PASSWORD)] = password;
			}
			authenticated.setPassword(password);
		} else {
			if (Objects.nonNull(password)) {
				event.getState()[properties.indexOf(AUTHENTICATED_PASSWORD)] = password;
			}
		}
		return false;
	}

	@Override
	public void onPreLoad(PreLoadEvent arg0) {
	}

}
