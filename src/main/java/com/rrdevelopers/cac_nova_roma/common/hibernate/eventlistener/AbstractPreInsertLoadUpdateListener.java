package com.rrdevelopers.cac_nova_roma.common.hibernate.eventlistener;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.hibernate.event.spi.AbstractEvent;
import org.hibernate.event.spi.AbstractPreDatabaseOperationEvent;
import org.hibernate.event.spi.PreInsertEvent;
import org.hibernate.event.spi.PreInsertEventListener;
import org.hibernate.event.spi.PreLoadEvent;
import org.hibernate.event.spi.PreLoadEventListener;
import org.hibernate.event.spi.PreUpdateEvent;
import org.hibernate.event.spi.PreUpdateEventListener;
import org.springframework.security.core.context.SecurityContextHolder;

import com.rrdevelopers.cac_nova_roma.common.security.model.LoggedUser;
import com.rrdevelopers.cac_nova_roma.control.model.User;

/**
 * @author rubens.ferreira 
 */
public abstract class AbstractPreInsertLoadUpdateListener
		implements PreInsertEventListener, PreUpdateEventListener, PreLoadEventListener {

	private static final long serialVersionUID = -392348078982918542L;
	protected static final String BASE_ENTITY_DATE_INSERT = "dateInsert";
	protected static final String BASE_ENTITY_DATE_UPDATE = "dateUpdate";
	protected static final String BASE_ENTITY_USER_INSERT = "userInsert";
	protected static final String BASE_ENTITY_USER_UPDATE = "userUpdate";
	protected static final String AUTHENTICATED_PASSWORD = "password";
	protected static final String AUTHENTICATED_LOGIN = "login";

	@Override
	public abstract boolean onPreInsert(PreInsertEvent event);

	@Override
	public abstract boolean onPreUpdate(PreUpdateEvent event);

	@Override
	public abstract void onPreLoad(PreLoadEvent arg0);

	protected User getAuthenticatedUser() {
		User result = null;

		if (Objects.nonNull(SecurityContextHolder.getContext().getAuthentication())) {
			LoggedUser loggedUser = (LoggedUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if (Objects.nonNull(loggedUser)) {
				result = loggedUser.getUser();
			}
		}

		return result;
	}

	protected List<String> listProperties(AbstractEvent event) {
		String[] properties = {};
		if (event instanceof AbstractPreDatabaseOperationEvent) {
			properties = ((AbstractPreDatabaseOperationEvent) event).getPersister().getEntityMetamodel()
					.getPropertyNames();
		} else {
			properties = ((PreLoadEvent) event).getPersister().getEntityMetamodel().getPropertyNames();
		}
		return Arrays.asList(properties);
	}

}
