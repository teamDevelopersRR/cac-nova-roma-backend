package com.rrdevelopers.cac_nova_roma.common.hibernate;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManagerFactory;

import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.rrdevelopers.cac_nova_roma.common.hibernate.eventlistener.AuthenticatedPreInsertLoadUpdateEventListener;
import com.rrdevelopers.cac_nova_roma.common.hibernate.eventlistener.BaseEntityPreInsertUpdateEventListener;
import com.rrdevelopers.cac_nova_roma.common.hibernate.eventlistener.PreInsertLoadUpdateEventListener;
import com.rrdevelopers.cac_nova_roma.control.model.Authenticated;
import com.rrdevelopers.cac_nova_roma.control.model.GenericEntity;

/**
 * @author rubens.ferreira Adiciona os listeners
 */
@Configuration
public class HibernateEvent {

	@Autowired
	private EntityManagerFactory entityManagerFactory;

	@Autowired
	private PreInsertLoadUpdateEventListener preInsertLoadUpdateEventListener;

	@PostConstruct
	public void registerListeners() {
		SessionFactoryImpl sessionFactory = entityManagerFactory.unwrap(SessionFactoryImpl.class);
		EventListenerRegistry registry = sessionFactory.getServiceRegistry().getService(EventListenerRegistry.class);
		registry.getEventListenerGroup(EventType.PRE_INSERT).appendListener(preInsertLoadUpdateEventListener);
		registry.getEventListenerGroup(EventType.PRE_UPDATE).appendListener(preInsertLoadUpdateEventListener);
		registry.getEventListenerGroup(EventType.PRE_LOAD).appendListener(preInsertLoadUpdateEventListener);

		preInsertLoadUpdateEventListener.addPreInsertEventListeners(GenericEntity.class,
				BaseEntityPreInsertUpdateEventListener.class);
		preInsertLoadUpdateEventListener.addPreUpdateEventListeners(GenericEntity.class,
				BaseEntityPreInsertUpdateEventListener.class);

		preInsertLoadUpdateEventListener.addPreLoadEventListeners(Authenticated.class,
				AuthenticatedPreInsertLoadUpdateEventListener.class);
		preInsertLoadUpdateEventListener.addPreInsertEventListeners(Authenticated.class,
				AuthenticatedPreInsertLoadUpdateEventListener.class);
		preInsertLoadUpdateEventListener.addPreUpdateEventListeners(Authenticated.class,
				AuthenticatedPreInsertLoadUpdateEventListener.class);

	}
}
