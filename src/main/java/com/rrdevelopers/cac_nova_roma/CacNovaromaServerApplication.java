package com.rrdevelopers.cac_nova_roma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CacNovaromaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CacNovaromaServerApplication.class, args);
	}
}
