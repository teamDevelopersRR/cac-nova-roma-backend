package com.rrdevelopers.cac_nova_roma.control.service;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rrdevelopers.cac_nova_roma.control.dao.IProfileDAO;
import com.rrdevelopers.cac_nova_roma.control.model.Profile;
import com.rrdevelopers.cac_nova_roma.control.model.ProfilePermission;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IProfilePermissionService;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IProfileService;

/**
 * @author Renatão
 *
 */
@Service
@Transactional
public class ProfileServiceImpl extends ServiceImpl<Profile, IProfileDAO> implements IProfileService {

	@Autowired
	private IProfileDAO dao;

	@Autowired
	private IProfilePermissionService profilePermissionService;

	@Override
	public List<Profile> getByNameContainsIgnoreCase(String name) {
		return dao.getByNameContainsIgnoreCase(name);
	}

	@Override
	public List<Profile> findAllByOrderByNameAsc() {
		return dao.findAllByOrderByNameAsc();
	}

	@Override
	public Profile save(Profile t) {
		Profile profile = super.save(t);
		profile.getProfilePermissions().forEach(pp -> this.saveProfilePermissions(pp, profile));
		return profile;
	}

	private void saveProfilePermissions(ProfilePermission profilePermission, Profile profile) {
		if (Objects.isNull(profilePermission.getProfile())) {
			profilePermission.setProfile(profile);
			profilePermissionService.save(profilePermission);
		}
	}

}
