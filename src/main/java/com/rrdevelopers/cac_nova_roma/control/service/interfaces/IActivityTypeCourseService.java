package com.rrdevelopers.cac_nova_roma.control.service.interfaces;

import java.util.List;

import com.rrdevelopers.cac_nova_roma.control.model.ActivityTypeCourse;

/**
 * @author Renatão
 *
 */
public interface IActivityTypeCourseService extends IService<ActivityTypeCourse>{
	
	ActivityTypeCourse getActivityTypeCourse(int activityTypeId, int courseId, String semesterDs);
	
	List<ActivityTypeCourse> getByActivityType(int activityTypeId);
	
	List<ActivityTypeCourse> getActivityTypeCourseByActivityTypeAndCourse(int idActivityType, int idCourse);

}
