package com.rrdevelopers.cac_nova_roma.control.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rrdevelopers.cac_nova_roma.control.dao.IDisciplineDAO;
import com.rrdevelopers.cac_nova_roma.control.model.Discipline;
import com.rrdevelopers.cac_nova_roma.control.service.ServiceImpl;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IDisciplineService;

/**
 * @author Renatão
 *
 */
@Service
@Transactional
public class DisciplineServiceImpl extends ServiceImpl<Discipline, IDisciplineDAO> implements IDisciplineService {

	@Autowired
	private IDisciplineDAO dao;
	
	@Override
	public int getSumHoursAllDisciplines() {
		return dao.getSumHoursAllDisciplines();
	}

	@Override
	public List<Discipline> getByNameContainsIgnoreCaseOrderByNameAsc(String name) {
		return dao.getByNameContainsIgnoreCaseOrderByNameAsc(name);
	}

	@Override
	public List<Discipline> findAllByOrderByNameAsc(){
		return dao.findAllByOrderByNameAsc();
	}

}
