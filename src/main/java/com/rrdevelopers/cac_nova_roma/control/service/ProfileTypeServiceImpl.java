package com.rrdevelopers.cac_nova_roma.control.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rrdevelopers.cac_nova_roma.control.dao.IProfileTypeDAO;
import com.rrdevelopers.cac_nova_roma.control.model.ProfileType;
import com.rrdevelopers.cac_nova_roma.control.service.ServiceImpl;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IProfileTypeService;

/**
 * @author Renatão
 *
 */
@Service
@Transactional
public class ProfileTypeServiceImpl extends ServiceImpl<ProfileType, IProfileTypeDAO> implements IProfileTypeService {

	@Autowired
	private IProfileTypeDAO dao;

	@Override
	public List<ProfileType> findAllByOrderByNameAsc(){
		return dao.findAllByOrderByNameAsc();
	}

}
