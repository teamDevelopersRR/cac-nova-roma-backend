package com.rrdevelopers.cac_nova_roma.control.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rrdevelopers.cac_nova_roma.control.dao.ICourseDAO;
import com.rrdevelopers.cac_nova_roma.control.model.Course;
import com.rrdevelopers.cac_nova_roma.control.service.ServiceImpl;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.ICourseService;

/**
 * @author RenatÃ£o
 *
 */
@Service
@Transactional
public class CourseServiceImpl extends ServiceImpl<Course, ICourseDAO> implements ICourseService {

	@Autowired
	private ICourseDAO dao;

	@Override
	public List<Course> getByNameContainsIgnoreCaseOrderByNameAsc(String name) {
		return dao.getByNameContainsIgnoreCaseOrderByNameAsc(name);
	}

	@Override
	public List<Course> getByCodeIgnoreCaseOrderByNameAsc(String code) {
		return dao.getByCodeIgnoreCaseOrderByNameAsc(code);
	}

	@Override
	public List<Course> findAllByOrderByNameAsc(){
		return dao.findAllByOrderByNameAsc();
	}
	
}
