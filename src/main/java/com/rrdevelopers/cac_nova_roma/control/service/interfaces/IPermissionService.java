package com.rrdevelopers.cac_nova_roma.control.service.interfaces;

import java.util.List;

import com.rrdevelopers.cac_nova_roma.control.model.Permission;

/**
 * @author Renatão
 *
 */
public interface IPermissionService extends IService<Permission>{
	
	List<Permission> getByNameContainsIgnoreCaseOrderByNameAsc(String name);
	
	Permission findByName(String name);
	
	boolean nameIsUnique(long id, String name);
	
	List<Permission> findAllByOrderByNameAsc();
	
}
