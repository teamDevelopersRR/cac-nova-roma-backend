package com.rrdevelopers.cac_nova_roma.control.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rrdevelopers.cac_nova_roma.control.dao.IActivityTypeCourseDAO;
import com.rrdevelopers.cac_nova_roma.control.model.ActivityTypeCourse;
import com.rrdevelopers.cac_nova_roma.control.service.ServiceImpl;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IActivityTypeCourseService;

/**
 * @author Renatão
 *
 */
@Service
@Transactional
public class ActivityTypeCourseServiceImpl extends ServiceImpl<ActivityTypeCourse, IActivityTypeCourseDAO> implements IActivityTypeCourseService {

	@Autowired
	IActivityTypeCourseDAO dao;
	
	@Override
	public ActivityTypeCourse getActivityTypeCourse(int activityTypeId, int courseId, String semesterDs) {
		return dao.getActivityTypeCourse(activityTypeId, courseId, semesterDs);
	}
	
	@Override
	public List<ActivityTypeCourse> getByActivityType(int activityTypeId) {
		return dao.getByActivityType(activityTypeId);
	}
	
	@Override
	public List<ActivityTypeCourse> getActivityTypeCourseByActivityTypeAndCourse(int idActivityType, int idCourse) {
		return dao.getActivityTypeCourseByActivityTypeAndCourse(idActivityType, idCourse);
	}

}
