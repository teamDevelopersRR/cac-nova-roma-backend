package com.rrdevelopers.cac_nova_roma.control.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rrdevelopers.cac_nova_roma.control.dao.IActivityDAO;
import com.rrdevelopers.cac_nova_roma.control.model.Activity;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IActivityService;

/**
 * @author rubens_ferreira
 *
 */
@Service
@Transactional
public class ActivityServiceImpl extends ServiceImpl<Activity, IActivityDAO> implements IActivityService {

	@Autowired
	private IActivityDAO dao;

	@Override
	public Activity save(Activity t) {
		Activity activity = super.saveAndFlush(t);
		return activity;
	}

	@Override
	public int getSumHoursAccountedByStudentAndActivityType(int userId, int activityTypeId) {
		return dao.getSumHoursAccountedByStudentAndActivityType(userId, activityTypeId);
	}

	@Override
	public int getSumHoursAccountedByStudent(int userId) {
		return dao.getSumHoursAccountedByStudent(userId);
	}

	@Override
	public int getSumOccurenceAccountedByStudentAndActivityType(int userId, int activityTypeId) {
		return dao.getSumOccurenceAccountedByStudentAndActivityType(userId, activityTypeId);
	}

	@Override
	public List<Activity> findAllByActiveTrueOrderByDateActivityAsc() {
		return dao.findAllByActiveTrueOrderByDateActivityAsc();
	}

	@Override
	public List<Activity> findByStudentId(Integer studentId) {
		return dao.findByStudentId(studentId);
	}

	@Override
	public List<Activity> findByStudentIdAndActiveTrueOrderByDateActivityAsc(Integer studentId) {
		return dao.findByStudentIdAndActiveTrueOrderByDateActivityAsc(studentId);
	}

}
