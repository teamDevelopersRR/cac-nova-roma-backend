package com.rrdevelopers.cac_nova_roma.control.service.interfaces;

import com.rrdevelopers.cac_nova_roma.control.model.UserTrace;

/**
 * @author rubens_ferreira
 *
 */
public interface IUserTraceService extends IService<UserTrace> {

}
