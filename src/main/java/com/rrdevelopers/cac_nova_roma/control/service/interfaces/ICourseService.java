package com.rrdevelopers.cac_nova_roma.control.service.interfaces;

import java.util.List;

import com.rrdevelopers.cac_nova_roma.control.model.Course;

/**
 * @author RenatÃ£o
 *
 */
public interface ICourseService extends IService<Course>{
	
	List<Course> getByNameContainsIgnoreCaseOrderByNameAsc(String name);
	
	List<Course> getByCodeIgnoreCaseOrderByNameAsc(String code);
	
	List<Course> findAllByOrderByNameAsc();

}
