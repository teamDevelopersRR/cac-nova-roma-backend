package com.rrdevelopers.cac_nova_roma.control.service;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rrdevelopers.cac_nova_roma.control.dao.IActivityTypeDAO;
import com.rrdevelopers.cac_nova_roma.control.model.ActivityType;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IActivityTypeService;

/**
 * @author Renatão
 *
 */
@Service
@Transactional
public class ActivityTypeServiceImpl extends ServiceImpl<ActivityType, IActivityTypeDAO> implements IActivityTypeService {

	@Autowired
	private IActivityTypeDAO dao;

	@Override
	public ActivityType findByName(String name) {
		return dao.findByName(name);
	}

	@Override
	public boolean nameIsUnique(long id, String name) {
		ActivityType activityType = this.findByName(name);

		return Objects.nonNull(activityType) ? (id == activityType.getId()) : true;
	}

	@Override
	public List<ActivityType> findAllByOrderByNameAsc(){
		return dao.findAllByOrderByNameAsc();
	}

}
