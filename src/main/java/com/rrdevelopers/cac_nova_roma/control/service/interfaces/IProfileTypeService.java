package com.rrdevelopers.cac_nova_roma.control.service.interfaces;

import java.util.List;

import com.rrdevelopers.cac_nova_roma.control.model.ProfileType;

/**
 * @author Renatão
 *
 */
public interface IProfileTypeService extends IService<ProfileType>{
	
	List<ProfileType> findAllByOrderByNameAsc();
	
}
