package com.rrdevelopers.cac_nova_roma.control.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rrdevelopers.cac_nova_roma.control.dao.IProfilePermissionDAO;
import com.rrdevelopers.cac_nova_roma.control.model.ProfilePermission;
import com.rrdevelopers.cac_nova_roma.control.service.ServiceImpl;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IProfilePermissionService;

/**
 * @author Renatão
 *
 */
@Service
@Transactional
public class ProfilePermissionServiceImpl extends ServiceImpl<ProfilePermission, IProfilePermissionDAO> implements IProfilePermissionService {

	@Autowired
	private IProfilePermissionDAO dao;

	@Override
	public List<ProfilePermission> getByProfile(Integer profileId) {
		return dao.getByProfile(profileId);
	}
	
	@Override
	public boolean deleteById(Integer id) {
		return dao.deleteById(id) == 1 ? true : false;
	}

	@Override
	public List<ProfilePermission> findAllByOrderByPermissionNameAsc() {
		return dao.findAllByOrderByPermissionNameAsc();
	}
	
	@Override
	public ProfilePermission findByProfileIdAndPermissionId(int profileId, int permissionId) {
		return dao.findByProfileIdAndPermissionId(profileId, permissionId);
	}

}
