package com.rrdevelopers.cac_nova_roma.control.service.interfaces;

import java.util.List;

import com.rrdevelopers.cac_nova_roma.control.model.Discipline;

/**
 * @author Renatão
 *
 */
public interface IDisciplineService extends IService<Discipline>{
	
	int getSumHoursAllDisciplines();
	
	List<Discipline> getByNameContainsIgnoreCaseOrderByNameAsc(String name);
	
	List<Discipline> findAllByOrderByNameAsc();

}
