package com.rrdevelopers.cac_nova_roma.control.service.interfaces;

import java.util.List;

import com.rrdevelopers.cac_nova_roma.control.model.Profile;

/**
 * @author Renatão
 *
 */
public interface IProfileService extends IService<Profile>{
	
	List<Profile> getByNameContainsIgnoreCase(String name);
	
	List<Profile> findAllByOrderByNameAsc();

}
