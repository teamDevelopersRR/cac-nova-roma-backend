package com.rrdevelopers.cac_nova_roma.control.service.interfaces;

import java.util.List;

import com.rrdevelopers.cac_nova_roma.control.model.Activity;

/**
 * @author rubens_ferreira
 *
 */
public interface IActivityService extends IService<Activity>{
	
	List<Activity> findAllByActiveTrueOrderByDateActivityAsc();
	
	int getSumHoursAccountedByStudentAndActivityType(int userId, int activityTypeId);
	
	int getSumHoursAccountedByStudent(int userId);
	
	int getSumOccurenceAccountedByStudentAndActivityType(int userId, int activityTypeId);
	
	List<Activity> findByStudentId(Integer studentId);
	
	List<Activity> findByStudentIdAndActiveTrueOrderByDateActivityAsc(Integer studentId);
	
}
