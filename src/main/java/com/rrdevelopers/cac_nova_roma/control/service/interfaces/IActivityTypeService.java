package com.rrdevelopers.cac_nova_roma.control.service.interfaces;

import java.util.List;

import com.rrdevelopers.cac_nova_roma.control.model.ActivityType;

/**
 * @author Renatão
 *
 */
public interface IActivityTypeService extends IService<ActivityType>{
	
	ActivityType findByName(String name);
	
	boolean nameIsUnique(long id, String name);
	
	List<ActivityType> findAllByOrderByNameAsc();

}
