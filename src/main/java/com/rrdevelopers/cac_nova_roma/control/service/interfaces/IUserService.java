package com.rrdevelopers.cac_nova_roma.control.service.interfaces;

import java.util.List;

import com.rrdevelopers.cac_nova_roma.control.model.User;
import com.rrdevelopers.cac_nova_roma.control.model.enums.ProfileCategoryEnum;

/**
 * @author rubens_ferreira
 *
 */
public interface IUserService extends IService<User> {

	User findByLoginAndPassword(String login, String password);
	
	User findByLogin(String login);
	
	List<User> findByLoginContainsAndTypeProfile(String login, ProfileCategoryEnum type);
	
	List<User> findAllByOrderByNameAsc();
	
}
