package com.rrdevelopers.cac_nova_roma.control.service.interfaces;

import java.util.List;

import com.rrdevelopers.cac_nova_roma.control.model.ActivityFile;

/**
 * @author rubens.ferreira
 *
 */
public interface IActivityFileService extends IService<ActivityFile>{
	
	List<ActivityFile> findByActivityId(long idActivity);

	List<ActivityFile> findAllByOrderByNameAsc();
	
}
