package com.rrdevelopers.cac_nova_roma.control.service.interfaces;

import java.util.List;

import com.rrdevelopers.cac_nova_roma.control.model.ActivityHistory;

/**
 * @author Renatão
 *
 */
public interface IActivityHistoryService extends IService<ActivityHistory>{

	List<ActivityHistory> findByActivityId(Integer activityId);
}
