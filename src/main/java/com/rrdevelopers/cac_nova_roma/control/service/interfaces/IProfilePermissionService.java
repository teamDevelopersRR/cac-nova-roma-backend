package com.rrdevelopers.cac_nova_roma.control.service.interfaces;

import java.util.List;

import com.rrdevelopers.cac_nova_roma.control.model.ProfilePermission;

/**
 * @author Renatão
 *
 */
public interface IProfilePermissionService extends IService<ProfilePermission>{
	
	List<ProfilePermission> getByProfile(Integer profileId);
	
	List<ProfilePermission> findAllByOrderByPermissionNameAsc();
	
	ProfilePermission findByProfileIdAndPermissionId(int profileId, int permissionId);

}
