package com.rrdevelopers.cac_nova_roma.control.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rrdevelopers.cac_nova_roma.control.dao.IUserTraceDAO;
import com.rrdevelopers.cac_nova_roma.control.model.UserTrace;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IUserTraceService;

/**
 * @author Renatão
 *
 */
@Service
@Transactional
public class UserTraceServiceImpl extends ServiceImpl<UserTrace, IUserTraceDAO> implements IUserTraceService {

}
