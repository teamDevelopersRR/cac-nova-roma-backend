package com.rrdevelopers.cac_nova_roma.control.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rrdevelopers.cac_nova_roma.control.dao.IActivityFileDAO;
import com.rrdevelopers.cac_nova_roma.control.model.ActivityFile;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IActivityFileService;

/**
 * @author rubens.ferreira
 *
 */
@Service
@Transactional
public class ActivityFileServiceImpl extends ServiceImpl<ActivityFile, IActivityFileDAO> implements IActivityFileService {

	@Autowired
	private IActivityFileDAO dao;

	@Override
	public List<ActivityFile> findByActivityId(long idActivity) {
		return dao.findByActivityId(idActivity);
	}

	@Override
	public List<ActivityFile> findAllByOrderByNameAsc(){
		return dao.findAllByOrderByNameAsc();
	}

}
