package com.rrdevelopers.cac_nova_roma.control.service.interfaces;

import java.util.List;

import com.rrdevelopers.cac_nova_roma.control.model.GenericEntity;

/**
 * @author rubens_ferreira
 *
 */
public interface IService<T extends GenericEntity> {

	T save(T t);
	
	T saveAndFlush(T t);

	List<T> save(List<T> tList);

	boolean delete(T t);

	T findById(Integer id);

	void deleteAll();

	long count();

	List<T> findAll();

	boolean deleteById(Integer id);
}
