package com.rrdevelopers.cac_nova_roma.control.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rrdevelopers.cac_nova_roma.control.model.GenericEntity;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IService;

/**
 * @author rubens_ferreira
 *
 */
@Service
@Transactional
public abstract class ServiceImpl<T extends GenericEntity, IDao extends JpaRepository<T, Serializable>> implements IService<T> {

	@Autowired
	private IDao dao;

	@Override
	public T save(T t) {
		return dao.save(t);
	}
	
	@Override
	public T saveAndFlush(T t) {
		return dao.saveAndFlush(t);
	}

	@Override
	public List<T> save(List<T> tList) {
		return (List<T>) dao.save(tList);
	}

	@Override
	public boolean delete(T t) {
		dao.delete(t);
		return dao.findOne(t.getId()) == null ? true : false;
	}

	@Override
	public T findById(Integer id) {
		return dao.findOne(id);
	}

	@Override
	public void deleteAll() {
		dao.deleteAll();
	}

	@Override
	public long count() {
		return dao.count();
	}

	@Override
	public List<T> findAll() {
		return (List<T>) dao.findAll();
	}

	@Override
	public boolean deleteById(Integer id) {
		dao.delete(id);
		return dao.findOne(id) == null ? true : false;
	}

}
