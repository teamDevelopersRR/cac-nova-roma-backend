package com.rrdevelopers.cac_nova_roma.control.service.interfaces;

import java.util.List;

import com.rrdevelopers.cac_nova_roma.control.model.CourseDiscipline;

/**
 * @author Renatão
 *
 */
public interface ICourseDisciplineService extends IService<CourseDiscipline>{

	List<CourseDiscipline> getByCourse(Integer courseId);

	List<CourseDiscipline> getCourseDisciplineByCourseAndDiscipline(Integer idCourse, Integer idDiscipline);

}
