package com.rrdevelopers.cac_nova_roma.control.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rrdevelopers.cac_nova_roma.control.dao.ICourseDisciplineDAO;
import com.rrdevelopers.cac_nova_roma.control.model.CourseDiscipline;
import com.rrdevelopers.cac_nova_roma.control.service.ServiceImpl;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.ICourseDisciplineService;

/**
 * @author Renatão
 *
 */
@Service
@Transactional
public class CourseDisciplineServiceImpl extends ServiceImpl<CourseDiscipline, ICourseDisciplineDAO> implements ICourseDisciplineService {

	@Autowired
	private ICourseDisciplineDAO dao;

	@Override
	public List<CourseDiscipline> getByCourse(Integer courseId) {
		return dao.getByCourse(courseId);
	}

	@Override
	public List<CourseDiscipline> getCourseDisciplineByCourseAndDiscipline(Integer idCourse, Integer idDiscipline) {
		return dao.getCourseDisciplineByCourseAndDiscipline(idCourse, idDiscipline);
	}

}
