package com.rrdevelopers.cac_nova_roma.control.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rrdevelopers.cac_nova_roma.control.dao.IActivityHistoryDAO;
import com.rrdevelopers.cac_nova_roma.control.model.ActivityHistory;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IActivityHistoryService;

/**
 * @author Renatão
 *
 */
@Service
@Transactional
public class ActivityHistoryServiceImpl extends ServiceImpl<ActivityHistory, IActivityHistoryDAO> implements IActivityHistoryService {

	@Autowired
	private IActivityHistoryDAO dao;
	
	@Override
	public List<ActivityHistory> findByActivityId(Integer activityId){
		return dao.findByActivityId(activityId);
	}
}
