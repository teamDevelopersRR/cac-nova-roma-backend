package com.rrdevelopers.cac_nova_roma.control.service;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rrdevelopers.cac_nova_roma.control.dao.IPermissionDAO;
import com.rrdevelopers.cac_nova_roma.control.model.Permission;
import com.rrdevelopers.cac_nova_roma.control.service.ServiceImpl;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IPermissionService;

/**
 * @author Renatão
 *
 */
@Service
@Transactional
public class PermissionServiceImpl extends ServiceImpl<Permission, IPermissionDAO> implements IPermissionService {

	@Autowired
	private IPermissionDAO dao;

	@Override
	public List<Permission> getByNameContainsIgnoreCaseOrderByNameAsc(String name) {
		return dao.getByNameContainsIgnoreCaseOrderByNameAsc(name);
	}

	@Override
	public List<Permission> findAllByOrderByNameAsc(){
		return dao.findAllByOrderByNameAsc();
	}
	
	@Override
	public Permission findByName(String name) {
		return dao.findByName(name);
	}

	@Override
	public boolean nameIsUnique(long id, String name) {
		Permission permission = this.findByName(name);

		return Objects.nonNull(permission) ? (id == permission.getId()) : true;
	}

}
