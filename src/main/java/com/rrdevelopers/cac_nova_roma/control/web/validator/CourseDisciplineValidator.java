/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.web.validator;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.rrdevelopers.cac_nova_roma.common.validator.CACValidator;
import com.rrdevelopers.cac_nova_roma.control.model.CourseDiscipline;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.ICourseDisciplineService;

/**
 * @author Renatão
 *
 */
@Component
public class CourseDisciplineValidator extends CACValidator<CourseDiscipline> {

	@Autowired
	private ICourseDisciplineService service;

	@Override
	public boolean supports(Class<?> clazz) {
		return CourseDiscipline.class.isAssignableFrom(clazz);
	}

	@Override
	public void validateEntity(Object object, Errors errors) {
		CourseDiscipline courseDiscipline = (CourseDiscipline) object;

		if (!objectIsNull(courseDiscipline, errors)) {

			// METODO PARA UPDATE
			if (courseDiscipline.getId() > 0) {
				if (!objectNotExist(service.findById(courseDiscipline.getId()), errors)) {

				}
			} else {
				List<CourseDiscipline> list = service.getCourseDisciplineByCourseAndDiscipline(courseDiscipline.getCourse().getId(), courseDiscipline.getDiscipline().getId());
				checkUniqueRegister("discipline", list, errors);
			}				
			
			isNumberInvalid("course", courseDiscipline.getCourse().getId(), errors);
			objectIsNull("course", courseDiscipline.getCourse(), errors);

			isNumberInvalid("discipline", courseDiscipline.getDiscipline().getId(), errors);
			objectIsNull("discipline", courseDiscipline.getDiscipline(), errors);
			
			
		}
	}

}
