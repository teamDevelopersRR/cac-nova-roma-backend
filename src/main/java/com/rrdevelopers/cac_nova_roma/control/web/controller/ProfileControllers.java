/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.web.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rrdevelopers.cac_nova_roma.common.handler.exceptions.GenericValidationException;
import com.rrdevelopers.cac_nova_roma.common.security.util.LoggedUserUtil;
import com.rrdevelopers.cac_nova_roma.control.model.Permission;
import com.rrdevelopers.cac_nova_roma.control.model.Profile;
import com.rrdevelopers.cac_nova_roma.control.model.ProfilePermission;
import com.rrdevelopers.cac_nova_roma.control.model.ProfileType;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IPermissionService;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IProfileService;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IProfileTypeService;
import com.rrdevelopers.cac_nova_roma.control.web.controller.dto.ProfilePermissionDto;
import com.rrdevelopers.cac_nova_roma.control.web.validator.ProfileValidator;

/**
 * @author Renatão
 *
 */
@RestController
@RequestMapping("/profile")
@PreAuthorize("hasRole('ROLE_PROFILE')")
public class ProfileControllers {
	// NÃO DEVE SER RENOMEADA A CLASSE
	// ProfileController conflita com o nome dessa classe
	// org.springframework.data.rest.webmvc.ProfileController dentro do FRAMEWORK
	// SPRING

	@Autowired
	private IProfileService service;
	
	@Autowired
	private IProfileTypeService profileTypeService;
	
	@Autowired
	private IPermissionService permissionService;

	@Autowired
	private ProfileValidator validator;

	@InitBinder
	protected void initiBinder(WebDataBinder binder, HttpServletRequest request) {
		validator.setMethod(RequestMethod.valueOf(request.getMethod()));
		binder.addValidators(validator);
	}

	@PostMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_PROFILE_INSERT')")
	public ResponseEntity<Profile> save(@RequestBody @Valid Profile profile, BindingResult result,
			HttpServletResponse response) {
		if (result.hasErrors()) {
			throw new GenericValidationException(result, "Error to Save Profile");
		}
		service.save(profile);
		return new ResponseEntity<Profile>(profile, HttpStatus.OK);
	}

	@PutMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_PROFILE_UPDATE')")
	public Profile update(@RequestBody @Valid Profile profile, BindingResult result, HttpServletResponse response) {
		if (result.hasErrors()) {
			throw new GenericValidationException(result, "Error to Update Profile");
		}
		service.save(profile);
		return profile;
	}

	@DeleteMapping(value = { "/{id}" })
	@PreAuthorize("hasRole('ROLE_PROFILE_DELETE')")
	public boolean delete(@PathVariable("id") Integer id) {
		try {
			Profile profile = service.findById(id);
			return service.delete(profile);
		} catch (Exception e) {
			throw new GenericValidationException(null, "Error to Delete Profile");
		}
	}

	@GetMapping("/{id}")
	@PreAuthorize("hasRole('ROLE_PROFILE_READ')")
	public ResponseEntity<Profile> findById(@PathVariable("id") Integer id) {
		return new ResponseEntity<Profile>(service.findById(id), HttpStatus.OK);
	}

	@GetMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_PROFILE_READ')")
	public ResponseEntity<List<Profile>> findAllByOrderByNameAsc() {
		return new ResponseEntity<List<Profile>>(service.findAllByOrderByNameAsc(), HttpStatus.OK);
	}

	@PostMapping(value = "/getByName")
	@PreAuthorize("hasRole('ROLE_PROFILE_READ')")
	public ResponseEntity<List<Profile>> getByNameContainsIgnoreCase(@RequestParam("name") String name) {
		return new ResponseEntity<List<Profile>>(service.getByNameContainsIgnoreCase(name), HttpStatus.OK);
	}

	@GetMapping("/permissions")
	public ResponseEntity<List<ProfilePermissionDto>> getProfilePermissions() {
		List<ProfilePermission> ppLoggedUser = LoggedUserUtil.getUser().getProfile().getProfilePermissions();
		List<Permission> pLoggedUser = ppLoggedUser.stream().map(ProfilePermission::getPermission)
				.collect(Collectors.toList());
		List<Permission> allP = permissionService.findAll();

		return new ResponseEntity<List<ProfilePermissionDto>>(allP.stream().map(permission -> {
			if (pLoggedUser.contains(permission)) {
				ProfilePermission ppFound = ppLoggedUser.stream().filter(pp -> pp.getPermission().equals(permission))
						.findFirst().get();
				if (permission.isCrud()) {
					return new ProfilePermissionDto(ppFound.getPermission(), ppFound.isRead(), ppFound.isInsert(),
							ppFound.isUpdate(), ppFound.isDelete());
				} else {
					return new ProfilePermissionDto(ppFound.getPermission(), true, false, false, false);
				}
			}
			;
			return new ProfilePermissionDto(permission);
		}).collect(Collectors.toList()), HttpStatus.OK);
	}

	@GetMapping("/permissions/{profileId}")
	public ResponseEntity<List<ProfilePermissionDto>> getProfilePermissionsByProfile(
			@PathVariable("profileId") int id) {
		List<ProfilePermissionDto> profilePermissions = service.findById(id).getProfilePermissions().stream()
				.map(profPerm -> new ProfilePermissionDto(profPerm)).collect(Collectors.toList());
		List<ProfilePermissionDto> all = getProfilePermissions().getBody();

		all.stream().forEach(profPermission -> {
			if (profilePermissions.contains(profPermission)) {
				ProfilePermissionDto ppFound = profilePermissions.stream()
						.filter(pp -> pp.getPermission().equals(profPermission.getPermission())).findFirst().get();

				if (ppFound.getPermission().isCrud()) {
					profPermission.setValuesCrud(ppFound.isRead(), ppFound.isInsert(), ppFound.isUpdate(),
							ppFound.isDelete());
				} else {
					profPermission.setRead(true);
				}
			}
		});

		return new ResponseEntity<List<ProfilePermissionDto>>(all, HttpStatus.OK);
	}

	@GetMapping("/profileTypes")
	public ResponseEntity<List<ProfileType>> findProfileTypesNameAsc() {
		return new ResponseEntity<List<ProfileType>>(profileTypeService.findAllByOrderByNameAsc(), HttpStatus.OK);
	}
}
