/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.web.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.rrdevelopers.cac_nova_roma.common.validator.CACValidator;
import com.rrdevelopers.cac_nova_roma.control.model.User;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IUserService;

/**
 * @author rubens.ferreira
 *
 */
@Component
public class UserValidator extends CACValidator<User> {

	@Autowired
	private IUserService userService;

	@Override
	public boolean supports(Class<?> clazz) {
		return User.class.isAssignableFrom(clazz);
	}

	@Override
	public void validateEntity(Object object, Errors errors) {
		User user = (User) object;

		if (!objectIsNull(user, errors)) {

			// METODO PARA UPDATE
			if (user.getId() > 0) {
				if (!objectNotExist(userService.findById(user.getId()), errors)) {

				}
			}
			
			isNumberInvalid("profile", user.getProfile().getId(), errors);
			objectIsNull("profile", user.getProfile(), errors);
			
			isNumberInvalid("course", user.getCourse().getId(), errors);
			objectIsNull("course", user.getCourse(), errors);
			
			if (!isNullOrEmpty("login", user.getLogin(), errors)) {
				checkSize("login", user.getLogin(), 1, 50, errors);
			}
			
			if (!isNullOrEmpty("name", user.getName(), errors)) {
				checkSize("name", user.getName(), 1, 1000, errors);
			}
			
			if (!isNullOrEmpty("cpf", user.getCpf(), errors)) {
				checkSize("cpf", user.getCpf(), 1, 14, errors);
			}
		}
	}

}
