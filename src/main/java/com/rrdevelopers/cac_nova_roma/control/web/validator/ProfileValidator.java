/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.web.validator;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.rrdevelopers.cac_nova_roma.common.validator.CACValidator;
import com.rrdevelopers.cac_nova_roma.control.model.Permission;
import com.rrdevelopers.cac_nova_roma.control.model.Profile;
import com.rrdevelopers.cac_nova_roma.control.model.ProfilePermission;
import com.rrdevelopers.cac_nova_roma.control.model.ProfileType;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IPermissionService;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IProfilePermissionService;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IProfileService;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IProfileTypeService;

/**
 * @author Renatão
 *
 */
@Component
public class ProfileValidator extends CACValidator<Profile> {

	@Autowired
	private IProfileService service;

	@Autowired
	private IProfileTypeService profileTypeService;
	
	@Autowired
	private IProfilePermissionService profilePermissionService;

	@Autowired
	private IPermissionService permissionService;

	@Override
	public boolean supports(Class<?> clazz) {
		return Profile.class.isAssignableFrom(clazz);
	}

	@Override
	public void validateEntity(Object object, Errors errors) {
		Profile profile = (Profile) object;

		if (!objectIsNull(profile, errors)) {

			// METODO PARA UPDATE
			if (profile.getId() > 0) {
				if (!objectNotExist(service.findById(profile.getId()), errors)) {

				}
			}

			if (!isNullOrEmpty("name", profile.getName(), errors)) {
				checkSize("name", profile.getName(), 1, 30, errors);
			}

			if (!isNumberInvalid("hierarchy", profile.getHierarchy(), errors)) {
				checkInterval("hierarchy", profile.getHierarchy(), 1, 3, errors);
			}

			isActivityTypeIsInvalid(profile.getType(), errors);

			if (isNotNullOrEmpty(profile.getProfilePermissions())) {
				profile.getProfilePermissions().forEach(profilePermission -> {
					Permission permission = permissionService.findById(profilePermission.getPermission().getId());
					if(!objectIsNull("profilePermissions", permission, errors)) {
						if(profile.getId() > 0) {
							ProfilePermission profilePermissionFound = profilePermissionService.findByProfileIdAndPermissionId(profile.getId(), permission.getId());
							if(Objects.nonNull(profilePermissionFound)) {
								profilePermission.setId(profilePermissionFound.getId());
								profilePermissionService.save(profilePermission);
							}
						}
					};
				});
			}

		}
	}

	private boolean isActivityTypeIsInvalid(ProfileType profileType, Errors errors) {
		ProfileType profileTypeFound = profileTypeService.findById(profileType.getId());

		if (Objects.isNull(profileTypeFound)) {
			errors.rejectValue("type", "profile.type.invalidObject", "Object not found");
			return true;
		}

		return false;
	}

}
