/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.web.validator;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.rrdevelopers.cac_nova_roma.common.validator.CACValidator;
import com.rrdevelopers.cac_nova_roma.control.model.Course;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.ICourseService;

/**
 * @author RenatÃ£o
 *
 */
@Component
public class CourseValidator extends CACValidator<Course> {

	@Autowired
	private ICourseService service;

	@Override
	public boolean supports(Class<?> clazz) {
		return Course.class.isAssignableFrom(clazz);
	}

	@Override
	public void validateEntity(Object object, Errors errors) {
		Course course = (Course) object;

		if (!objectIsNull(course, errors)) {

			// METODO PARA UPDATE
			if (course.getId() > 0) {
				if (!objectNotExist(service.findById(course.getId()), errors)) {

				}
			} else {
				List<Course> list = service.getByCodeIgnoreCaseOrderByNameAsc(course.getCode());
				checkUnique("code", list, errors);
			}
			
			if (!isNullOrEmpty("code", course.getCode(), errors)) {
				checkSize("code", course.getCode(), 1, 10, errors);
				
				containsSpace("code", course.getCode(), errors);
			}
			
			if (!isNullOrEmpty("name", course.getName(), errors)) {
				checkSize("name", course.getName(), 1, 1000, errors);
			}
			
			isNumberInvalid("numberHourMax", course.getNumberHourMax(), errors);
			
		}
	}

}
