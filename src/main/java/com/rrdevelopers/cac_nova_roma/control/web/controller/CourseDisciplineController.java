/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rrdevelopers.cac_nova_roma.common.handler.exceptions.GenericValidationException;
import com.rrdevelopers.cac_nova_roma.control.model.CourseDiscipline;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.ICourseDisciplineService;
import com.rrdevelopers.cac_nova_roma.control.web.validator.CourseDisciplineValidator;

/**
 * @author Renatão
 *
 */
@RestController
@RequestMapping("/courseDiscipline")
@PreAuthorize("hasRole('ROLE_COURSE_DISCIPLINE')")
public class CourseDisciplineController {

	@Autowired
	private ICourseDisciplineService service;

	@Autowired
	private CourseDisciplineValidator validator;

	@InitBinder
	protected void initiBinder(WebDataBinder binder, HttpServletRequest request) {
		validator.setMethod(RequestMethod.valueOf(request.getMethod()));
		binder.addValidators(validator);
	}

	@PostMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_COURSE_DISCIPLINE_INSERT')")
	public ResponseEntity<CourseDiscipline> save(@RequestBody @Valid CourseDiscipline courseDiscipline, BindingResult result,
			HttpServletResponse response) {
		if (result.hasErrors()) {
			throw new GenericValidationException(result, "Error to Save Course Discipline");
		}
		service.save(courseDiscipline);
		return new ResponseEntity<CourseDiscipline>(courseDiscipline, HttpStatus.OK);
	}

	@PutMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_COURSE_DISCIPLINE_UPDATE')")
	public CourseDiscipline update(@RequestBody @Valid CourseDiscipline courseDiscipline, BindingResult result, HttpServletResponse response) {
		if (result.hasErrors()) {
			throw new GenericValidationException(result, "Error to Update Course Discipline");
		}
		service.save(courseDiscipline);
		return courseDiscipline;
	}

	@DeleteMapping(value = { "/{id}" })
	@PreAuthorize("hasRole('ROLE_COURSE_DISCIPLINE_DELETE')")
	public boolean delete(@PathVariable("id") Integer id) {
		try{
			CourseDiscipline courseDiscipline = service.findById(id);
			return service.delete(courseDiscipline);
		} catch(Exception e){
			throw new GenericValidationException(null, "Error to Delete Course Discipline");
		}
	}

	@GetMapping("/{id}")
	@PreAuthorize("hasRole('ROLE_COURSE_DISCIPLINE_READ')")
	public ResponseEntity<CourseDiscipline> findById(@PathVariable("id") Integer id) {
		return new ResponseEntity<CourseDiscipline>(service.findById(id), HttpStatus.OK);
	}

	@GetMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_COURSE_DISCIPLINE_READ')")
	public ResponseEntity<List<CourseDiscipline>> getAll() {
		return new ResponseEntity<List<CourseDiscipline>>(service.findAll(), HttpStatus.OK);
	}

	@GetMapping(value = { "/getByCourse/{id}", "" })
	@PreAuthorize("hasRole('ROLE_COURSE_DISCIPLINE_READ')")
	public ResponseEntity<List<CourseDiscipline>> getByCourse(@PathVariable("id") Integer id) {
		return new ResponseEntity<List<CourseDiscipline>>(service.getByCourse(id), HttpStatus.OK);
	}

}
