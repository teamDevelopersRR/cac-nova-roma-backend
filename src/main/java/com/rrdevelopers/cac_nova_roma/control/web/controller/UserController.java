/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rrdevelopers.cac_nova_roma.common.handler.exceptions.GenericValidationException;
import com.rrdevelopers.cac_nova_roma.control.model.User;
import com.rrdevelopers.cac_nova_roma.control.model.enums.ProfileCategoryEnum;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IUserService;
import com.rrdevelopers.cac_nova_roma.control.web.validator.UserValidator;

/**
 * @author rubens.ferreira
 *
 */
@RestController
@RequestMapping("/user")
@PreAuthorize("hasRole('ROLE_STUDENT')")
public class UserController {

	@Autowired
	private IUserService service;

	@Autowired
	private UserValidator validator;

	@InitBinder
	protected void initiBinder(WebDataBinder binder, HttpServletRequest request) {
		validator.setMethod(RequestMethod.valueOf(request.getMethod()));
		binder.addValidators(validator);
	}

	@PostMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_STUDENT_INSERT')")
	public ResponseEntity<User> save(@RequestBody @Valid User user, BindingResult result,
			HttpServletResponse response) {
		if (result.hasErrors()) {
			throw new GenericValidationException(result, "Error to Save User");
		}
		service.save(user);
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	@PutMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_STUDENT_UPDATE')")
	public User update(@RequestBody @Valid User user, BindingResult result, HttpServletResponse response) {
		if (result.hasErrors()) {
			throw new GenericValidationException(result, "Error to Update User");
		}
		service.save(user);
		return user;
	}

	@DeleteMapping(value = { "/{id}" })
	@PreAuthorize("hasRole('ROLE_STUDENT_DELETE')")
	public boolean delete(@PathVariable("id") Integer id) {
		try{
			User user = service.findById(id);
			user.setProfile(null);
			user.setCourse(null);
			return service.delete(user);
		} catch(Exception e){
			throw new GenericValidationException(null, "Error to Delete User");
		}
	}

	@GetMapping("/{id}")
	@PreAuthorize("hasRole('ROLE_STUDENT_READ')")
	public ResponseEntity<User> findById(@PathVariable("id") Integer id) {
		return new ResponseEntity<User>(service.findById(id), HttpStatus.OK);
	}

	@GetMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_STUDENT_READ')")
	public ResponseEntity<List<User>> findAllByOrderByNameAsc() {
		return new ResponseEntity<List<User>>(service.findAllByOrderByNameAsc(), HttpStatus.OK);
	}

	@PostMapping(value = "/findByLoginContains")
	@PreAuthorize("hasRole('ROLE_STUDENT_READ')")
	public ResponseEntity<List<User>> findByLoginContains(@RequestParam("login") String login, @RequestParam("typeProfile") ProfileCategoryEnum type) {
		return new ResponseEntity<List<User>>(service.findByLoginContainsAndTypeProfile(login, type), HttpStatus.OK);
	}

}
