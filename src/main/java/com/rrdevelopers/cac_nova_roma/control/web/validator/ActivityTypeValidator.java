/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.web.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.rrdevelopers.cac_nova_roma.common.validator.CACValidator;
import com.rrdevelopers.cac_nova_roma.control.model.ActivityType;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IActivityTypeService;

/**
 * @author rubens.ferreira
 *
 */
@Component
public class ActivityTypeValidator extends CACValidator<ActivityType> {

	@Autowired
	private IActivityTypeService activityTypeService;

	@Override
	public boolean supports(Class<?> clazz) {
		return ActivityType.class.isAssignableFrom(clazz);
	}

	@Override
	public void validateEntity(Object object, Errors errors) {
		ActivityType activityType = (ActivityType) object;

		if (!objectIsNull(activityType, errors)) {

			// METODO PARA UPDATE
			if (activityType.getId() > 0) {
				if (!objectNotExist(activityTypeService.findById(activityType.getId()), errors)) {

				}
			}

			if (!isNullOrEmpty("name", activityType.getName(), errors)) {
				if(!activityTypeService.nameIsUnique(activityType.getId(), activityType.getName())) {
					repeatedField("name", activityType.getName(), errors);
				}
				checkSize("name", activityType.getName(), 2, 1000, errors);
			}
			
			if (!isNullOrEmpty(activityType.getDescription())) {
				checkSize("description", activityType.getName(), 2, 1000, errors);
			}
			
			isNullOrEmpty("category", activityType.getCategory().getText(), errors);
			
		}

	}

}
