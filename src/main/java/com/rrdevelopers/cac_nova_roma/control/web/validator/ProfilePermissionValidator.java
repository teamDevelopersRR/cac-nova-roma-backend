/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.web.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.rrdevelopers.cac_nova_roma.common.validator.CACValidator;
import com.rrdevelopers.cac_nova_roma.control.model.ProfilePermission;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IProfilePermissionService;

/**
 * @author Renatão
 *
 */
@Component
public class ProfilePermissionValidator extends CACValidator<ProfilePermission> {

	@Autowired
	private IProfilePermissionService service;

	@Override
	public boolean supports(Class<?> clazz) {
		return ProfilePermission.class.isAssignableFrom(clazz);
	}

	@Override
	public void validateEntity(Object object, Errors errors) {
		ProfilePermission profilePermission = (ProfilePermission) object;

		if (!objectIsNull(profilePermission, errors)) {
			
			// METODO PARA UPDATE
			if (profilePermission.getId() > 0) {
				if (!objectNotExist(service.findById(profilePermission.getId()), errors)) {

				}
			}

			isNumberInvalid("profile", profilePermission.getProfile().getId(), errors);
			objectIsNull("profile", profilePermission.getProfile(), errors);
			
			isNumberInvalid("permission", profilePermission.getPermission().getId(), errors);
			objectIsNull("permission", profilePermission.getPermission(), errors);
			
		}
	}

}
