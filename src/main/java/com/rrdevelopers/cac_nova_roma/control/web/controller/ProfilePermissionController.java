/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rrdevelopers.cac_nova_roma.common.handler.exceptions.GenericValidationException;
import com.rrdevelopers.cac_nova_roma.control.model.Profile;
import com.rrdevelopers.cac_nova_roma.control.model.ProfilePermission;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IProfilePermissionService;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IProfileService;
import com.rrdevelopers.cac_nova_roma.control.web.validator.ProfilePermissionValidator;

/**
 * @author Renatão
 *
 */
@RestController
@RequestMapping("/profilePermission")
@PreAuthorize("hasRole('ROLE_PROFILE_PERMISSION')")
public class ProfilePermissionController {

	@Autowired
	private IProfilePermissionService service;

	@Autowired
	private IProfileService profileService;

	@Autowired
	private ProfilePermissionValidator validator;

	@InitBinder
	protected void initiBinder(WebDataBinder binder, HttpServletRequest request) {
		validator.setMethod(RequestMethod.valueOf(request.getMethod()));
		binder.addValidators(validator);
	}

	@PostMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_PROFILE_PERMISSION_INSERT')")
	public ResponseEntity<ProfilePermission> save(@RequestBody @Valid ProfilePermission profilePermission,
			BindingResult result, HttpServletResponse response) {
		if (result.hasErrors()) {
			throw new GenericValidationException(result, "Error to Save Profile Permission");
		}
		service.save(profilePermission);
		return new ResponseEntity<ProfilePermission>(profilePermission, HttpStatus.OK);
	}

	@PutMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_PROFILE_PERMISSION_UPDATE')")
	public ProfilePermission update(@RequestBody @Valid ProfilePermission profilePermission, BindingResult result,
			HttpServletResponse response) {
		if (result.hasErrors()) {
			throw new GenericValidationException(result, "Error to Update Profile Permission");
		}
		service.save(profilePermission);
		return profilePermission;
	}

	@DeleteMapping(value = "/{id}")
	@PreAuthorize("hasRole('ROLE_PROFILE_PERMISSION_DELETE')")
	public boolean delete(@PathVariable("id") int id) {
		ProfilePermission profilePermission = service.findById(id);
		Profile profile = profileService.findById(profilePermission.getProfile().getId());

		profile.getProfilePermissions().remove(profilePermission);
		profileService.save(profile);
		return !profile.getProfilePermissions().contains(profilePermission);
	}

	@GetMapping("/{id}")
	@PreAuthorize("hasRole('ROLE_PROFILE_PERMISSION_READ')")
	public ResponseEntity<ProfilePermission> findById(@PathVariable("id") Integer id) {
		return new ResponseEntity<ProfilePermission>(service.findById(id), HttpStatus.OK);
	}

	@GetMapping(value = "/getByProfile/{id}")
	@PreAuthorize("hasRole('ROLE_PROFILE_PERMISSION_READ')")
	public ResponseEntity<List<ProfilePermission>> getByProfile(@PathVariable("id") Integer id) {
		return new ResponseEntity<List<ProfilePermission>>(service.getByProfile(id), HttpStatus.OK);
	}

	@GetMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_PROFILE_PERMISSION_READ')")
	public ResponseEntity<List<ProfilePermission>> findAllByOrderByPermissionNameAsc() {
		return new ResponseEntity<List<ProfilePermission>>(service.findAllByOrderByPermissionNameAsc(), HttpStatus.OK);
	}

}
