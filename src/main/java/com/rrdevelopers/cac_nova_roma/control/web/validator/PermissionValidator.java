/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.web.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.rrdevelopers.cac_nova_roma.common.validator.CACValidator;
import com.rrdevelopers.cac_nova_roma.control.model.Permission;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IPermissionService;

/**
 * @author Renatão
 *
 */
@Component
public class PermissionValidator extends CACValidator<Permission> {

	@Autowired
	private IPermissionService permissionService;

	@Override
	public boolean supports(Class<?> clazz) {
		return Permission.class.isAssignableFrom(clazz);
	}

	@Override
	public void validateEntity(Object object, Errors errors) {
		Permission permission = (Permission) object;

		if (!objectIsNull(permission, errors)) {

			// METODO PARA UPDATE
			if (permission.getId() > 0) {
				if (!objectNotExist(permissionService.findById(permission.getId()), errors)) {

				}
			}
			
			if (!isNullOrEmpty("name", permission.getName(), errors)) {
				checkSize("name", permission.getName(), 1, 50, errors);
				
				if(!permissionService.nameIsUnique(permission.getId(), permission.getName())) {
					repeatedField("name", permission.getName(), errors);
				}
			}
			
		}
	}

}
