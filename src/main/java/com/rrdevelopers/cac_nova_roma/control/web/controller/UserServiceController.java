/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.rrdevelopers.cac_nova_roma.control.model.User;
import com.rrdevelopers.cac_nova_roma.control.model.UserTrace;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IUserService;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IUserTraceService;

/**
 * @author Renatão
 *
 */
public class UserServiceController {

	@Autowired
	private IUserService userService;

	@Autowired
	private IUserTraceService userTraceService;

	public ResponseEntity<UserTrace> save(int idUser, String action, String ip) {
		User user = userService.findById(idUser);
		
		UserTrace userTrace = new UserTrace(action, ip, user);
		
		userTraceService.save(userTrace);
		return new ResponseEntity<UserTrace>(userTrace, HttpStatus.OK);
	}

}
