/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.web.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rrdevelopers.cac_nova_roma.common.handler.exceptions.GenericValidationException;
import com.rrdevelopers.cac_nova_roma.control.model.ActivityType;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IActivityTypeService;
import com.rrdevelopers.cac_nova_roma.control.web.validator.ActivityTypeValidator;

/**
 * @author rubens.ferreira
 *
 */
@RestController
@RequestMapping("/activityType")
@PreAuthorize("hasRole('ROLE_ACTIVITY_TYPE')")
public class ActivityTypeController {

	@Autowired
	private IActivityTypeService service;

	@Autowired
	private ActivityTypeValidator validator;

	@InitBinder
	protected void initiBinder(WebDataBinder binder, HttpServletRequest request) {
		validator.setMethod(RequestMethod.valueOf(request.getMethod()));
		binder.addValidators(validator);
	}

	@PostMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_ACTIVITY_TYPE_INSERT')")
	public ResponseEntity<ActivityType> save(@RequestBody @Valid ActivityType activityType, BindingResult result,
			HttpServletResponse response) {
		if (result.hasErrors()) {
			throw new GenericValidationException(result, "Error to Save Activity Type");
		}
		activityType = service.save(activityType);
		return new ResponseEntity<ActivityType>(activityType, HttpStatus.OK);
	}

	@PutMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_ACTIVITY_TYPE_UPDATE')")
	public ActivityType update(@RequestBody @Valid ActivityType activityType, BindingResult result,
			HttpServletResponse response) {
		if (result.hasErrors()) {
			throw new GenericValidationException(result, "Error to Update Activity Type");
		}
		service.save(activityType);
		return activityType;
	}

	@DeleteMapping(value = { "/{id}" })
	@PreAuthorize("hasRole('ROLE_ACTIVITY_TYPE_DELETE')")
	public boolean delete(@PathVariable("id") Integer id) {
		try {
			ActivityType activityType = service.findById(id);
			return service.delete(activityType);
		} catch (Exception e) {
			throw new GenericValidationException(null, "Error to Delete Activity Type");
		}
	}

	@GetMapping("/{id}")
	@PreAuthorize("hasRole('ROLE_ACTIVITY_TYPE_READ')")
	public ResponseEntity<ActivityType> findById(@PathVariable("id") Integer id) {
		return new ResponseEntity<ActivityType>(service.findById(id), HttpStatus.OK);
	}

	@GetMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_ACTIVITY_TYPE_READ')")
	public ResponseEntity<List<ActivityType>> findAllByOrderByNameAsc() {
		return new ResponseEntity<List<ActivityType>>(service.findAllByOrderByNameAsc(), HttpStatus.OK);
	}

	@GetMapping(value = "/dto")
	public ResponseEntity<List<ActivityType>> findDtoByOrderByNameAsc() {
		return new ResponseEntity<List<ActivityType>>(service.findAllByOrderByNameAsc().stream().map(type -> {
			ActivityType obj = new ActivityType();
			obj.setId(type.getId());
			obj.setName(type.getName());
			obj.setCategory(type.getCategory());
			return obj;
		}).collect(Collectors.toList()), HttpStatus.OK);
	}

}
