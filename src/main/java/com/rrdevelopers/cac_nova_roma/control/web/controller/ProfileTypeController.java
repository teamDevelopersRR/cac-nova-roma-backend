/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rrdevelopers.cac_nova_roma.common.handler.exceptions.GenericValidationException;
import com.rrdevelopers.cac_nova_roma.control.model.ProfileType;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IProfileTypeService;
import com.rrdevelopers.cac_nova_roma.control.web.validator.ProfileTypeValidator;

/**
 * @author Renatão
 *
 */
@RestController
@RequestMapping("/profileType")
@PreAuthorize("hasRole('ROLE_PROFILE_TYPE')")
public class ProfileTypeController {

	@Autowired
	private IProfileTypeService service;

	@Autowired
	private ProfileTypeValidator validator;

	@InitBinder
	protected void initiBinder(WebDataBinder binder, HttpServletRequest request) {
		validator.setMethod(RequestMethod.valueOf(request.getMethod()));
		binder.addValidators(validator);
	}

	@PostMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_PROFILE_TYPE_INSERT')")
	public ResponseEntity<ProfileType> save(@RequestBody @Valid ProfileType profileType, BindingResult result,
			HttpServletResponse response) {
		if (result.hasErrors()) {
			throw new GenericValidationException(result, "Error to Save Profile Type");
		}
		service.save(profileType);
		return new ResponseEntity<ProfileType>(profileType, HttpStatus.OK);
	}

	@PutMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_PROFILE_TYPE_UPDATE')")
	public ProfileType update(@RequestBody @Valid ProfileType profileType, BindingResult result, HttpServletResponse response) {
		if (result.hasErrors()) {
			throw new GenericValidationException(result, "Error to Update Profile Type");
		}
		service.save(profileType);
		return profileType;
	}

	@DeleteMapping(value = { "/{id}" })
	@PreAuthorize("hasRole('ROLE_PROFILE_TYPE_DELETE')")
	public boolean delete(@PathVariable("id") Integer id) {
		try{
			ProfileType profileType = service.findById(id);
			return service.delete(profileType);
		} catch(Exception e){
			throw new GenericValidationException(null, "Error to Delete Profile Type");
		}
	}

	@GetMapping("/{id}")
	@PreAuthorize("hasRole('ROLE_PROFILE_TYPE_READ')")
	public ResponseEntity<ProfileType> findById(@PathVariable("id") Integer id) {
		return new ResponseEntity<ProfileType>(service.findById(id), HttpStatus.OK);
	}

	@GetMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_PROFILE_TYPE_READ')")
	public ResponseEntity<List<ProfileType>> findAllByOrderByNameAsc() {
		return new ResponseEntity<List<ProfileType>>(service.findAllByOrderByNameAsc(), HttpStatus.OK);
	}

}
