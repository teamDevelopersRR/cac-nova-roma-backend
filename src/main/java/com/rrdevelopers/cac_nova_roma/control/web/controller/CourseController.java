/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rrdevelopers.cac_nova_roma.common.handler.exceptions.GenericValidationException;
import com.rrdevelopers.cac_nova_roma.control.model.Course;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.ICourseService;
import com.rrdevelopers.cac_nova_roma.control.web.validator.CourseValidator;

/**
 * @author Renatão
 *
 */
@RestController
@RequestMapping("/course")
@PreAuthorize("hasRole('ROLE_COURSE')")
public class CourseController {

	@Autowired
	private ICourseService service;

	@Autowired
	private CourseValidator validator;

	@InitBinder
	protected void initiBinder(WebDataBinder binder, HttpServletRequest request) {
		validator.setMethod(RequestMethod.valueOf(request.getMethod()));
		binder.addValidators(validator);
	}

	@PostMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_COURSE_INSERT')")
	public ResponseEntity<Course> save(@RequestBody @Valid Course course, BindingResult result,
			HttpServletResponse response) {
		if (result.hasErrors()) {
			throw new GenericValidationException(result, "Error to Save Course");
		}
		service.save(course);
		return new ResponseEntity<Course>(course, HttpStatus.OK);
	}

	@PutMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_COURSE_UPDATE')")
	public Course update(@RequestBody @Valid Course course, BindingResult result, HttpServletResponse response) {
		if (result.hasErrors()) {
			throw new GenericValidationException(result, "Error to Update Course");
		}
		service.save(course);
		return course;
	}

	@DeleteMapping(value = { "/{id}" })
	@PreAuthorize("hasRole('ROLE_COURSE_DELETE')")
	public boolean delete(@PathVariable("id") Integer id) {
		try{
			Course course = service.findById(id);
			return service.delete(course);
		} catch(Exception e){
			throw new GenericValidationException(null, "Error to Delete Course");
		}
	}

	@GetMapping("/{id}")
	@PreAuthorize("hasRole('ROLE_COURSE_READ')")
	public ResponseEntity<Course> findById(@PathVariable("id") Integer id) {
		return new ResponseEntity<Course>(service.findById(id), HttpStatus.OK);
	}

	@GetMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_COURSE_READ')")
	public ResponseEntity<List<Course>> findAllByOrderByNameAsc() {
		return new ResponseEntity<List<Course>>(service.findAllByOrderByNameAsc(), HttpStatus.OK);
	}

	@PostMapping(value = "/getByName")
	@PreAuthorize("hasRole('ROLE_COURSE_READ')")
	public ResponseEntity<List<Course>> getByNameContainsIgnoreCaseOrderByNameAsc(@RequestParam("name") String name) {
		return new ResponseEntity<List<Course>>(service.getByNameContainsIgnoreCaseOrderByNameAsc(name), HttpStatus.OK);
	}

}
