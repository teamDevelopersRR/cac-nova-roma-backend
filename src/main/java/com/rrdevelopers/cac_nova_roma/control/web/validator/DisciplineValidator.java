/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.web.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.rrdevelopers.cac_nova_roma.common.validator.CACValidator;
import com.rrdevelopers.cac_nova_roma.control.model.Discipline;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IDisciplineService;

/**
 * @author Renatão
 *
 */
@Component
public class DisciplineValidator extends CACValidator<Discipline> {

	@Autowired
	private IDisciplineService service;

	@Override
	public boolean supports(Class<?> clazz) {
		return Discipline.class.isAssignableFrom(clazz);
	}

	@Override
	public void validateEntity(Object object, Errors errors) {
		Discipline discipline = (Discipline) object;

		if (!objectIsNull(discipline, errors)) {

			// METODO PARA UPDATE
			if (discipline.getId() > 0) {
				if (!objectNotExist(service.findById(discipline.getId()), errors)) {

				}
			}
			
			if (!isNullOrEmpty("name", discipline.getName(), errors)) {
				checkSize("name", discipline.getName(), 1, 100, errors);
			}
			
			isNumberInvalid("numberHour", discipline.getNumberHour(), errors);
			
		}
	}

}
