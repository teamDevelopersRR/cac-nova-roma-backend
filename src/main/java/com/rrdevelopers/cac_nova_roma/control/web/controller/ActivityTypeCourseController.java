/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rrdevelopers.cac_nova_roma.common.handler.exceptions.GenericValidationException;
import com.rrdevelopers.cac_nova_roma.control.model.ActivityTypeCourse;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IActivityTypeCourseService;
import com.rrdevelopers.cac_nova_roma.control.web.validator.ActivityTypeCourseValidator;

/**
 * @author rubens.ferreira
 *
 */
@RestController
@RequestMapping("/activityTypeCourse")
@PreAuthorize("hasRole('ROLE_ACTIVITY_TYPE_COURSE')")
public class ActivityTypeCourseController {

	@Autowired
	private IActivityTypeCourseService service;

	@Autowired
	private ActivityTypeCourseValidator validator;

	@InitBinder
	protected void initiBinder(WebDataBinder binder, HttpServletRequest request) {
		validator.setMethod(RequestMethod.valueOf(request.getMethod()));
		binder.addValidators(validator);
	}

	@PostMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_ACTIVITY_TYPE_COURSE_INSERT')")
	public ResponseEntity<ActivityTypeCourse> save(@RequestBody @Valid ActivityTypeCourse activityTypeCourse, BindingResult result,
			HttpServletResponse response) {
		if (result.hasErrors()) {
			throw new GenericValidationException(result, "Error to Save Activity Type Course");
		}
		activityTypeCourse = service.save(activityTypeCourse);
		return new ResponseEntity<ActivityTypeCourse>(activityTypeCourse, HttpStatus.OK);
	}

	@PutMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_ACTIVITY_TYPE_COURSE_UPDATE')")
	public ActivityTypeCourse update(@RequestBody @Valid ActivityTypeCourse activityTypeCourse, BindingResult result,
			HttpServletResponse response) {
		if (result.hasErrors()) {
			throw new GenericValidationException(result, "Error to Update Activity Type Course");
		}
		service.save(activityTypeCourse);
		return activityTypeCourse;
	}

	@DeleteMapping(value = { "/{id}" })
	@PreAuthorize("hasRole('ROLE_ACTIVITY_TYPE_COURSE_DELETE')")
	public boolean delete(@PathVariable("id") Integer id) {
		try{
			ActivityTypeCourse activityTypeCourse = service.findById(id);
			return service.delete(activityTypeCourse);
		} catch(Exception e){
			throw new GenericValidationException(null, "Error to Delete Activity Type Course");
		}
	}

	@GetMapping("/getByActivityType/{id}")
	@PreAuthorize("hasRole('ROLE_ACTIVITY_TYPE_COURSE_READ')")
	public ResponseEntity<List<ActivityTypeCourse>> getByActivityType(@PathVariable("id") Integer id) {
		return new ResponseEntity<List<ActivityTypeCourse>>(service.getByActivityType(id), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	@PreAuthorize("hasRole('ROLE_ACTIVITY_TYPE_COURSE_READ')")
	public ResponseEntity<ActivityTypeCourse> findById(@PathVariable("id") Integer id) {
		return new ResponseEntity<ActivityTypeCourse>(service.findById(id), HttpStatus.OK);
	}

	@GetMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_ACTIVITY_TYPE_COURSE_READ')")
	public ResponseEntity<List<ActivityTypeCourse>> getAll() {
		return new ResponseEntity<List<ActivityTypeCourse>>(service.findAll(), HttpStatus.OK);
	}

}
