package com.rrdevelopers.cac_nova_roma.control.web.controller.dto;

import com.rrdevelopers.cac_nova_roma.control.model.Permission;
import com.rrdevelopers.cac_nova_roma.control.model.ProfilePermission;

public class ProfilePermissionDto {

	private PermissionDto permission;
	private boolean disabledRead, disabledInsert, disabledUpdate, disabledDelete;
	private boolean read, insert, update, delete;

	public ProfilePermissionDto(ProfilePermission profilePermission) {
		this.read = profilePermission.isRead();
		this.insert = profilePermission.isInsert();
		this.update = profilePermission.isUpdate();
		this.delete = profilePermission.isDelete();
		this.permission = new PermissionDto(profilePermission.getPermission().getId(), profilePermission.getPermission().isCrud(), profilePermission.getPermission().getName());
	}
	
	public ProfilePermissionDto(Permission permission, boolean canRead, boolean canInsert,
			boolean canUpdate, boolean canDelete) {
		this.permission = new PermissionDto(permission.getId(), permission.isCrud(), permission.getName());
		this.disabledRead = !canRead;
		this.disabledInsert = !canInsert;
		this.disabledUpdate = !canUpdate;
		this.disabledDelete = !canDelete;
	}
	
	public ProfilePermissionDto(Permission permission) {
		this.permission = new PermissionDto(permission.getId(), permission.isCrud(), permission.getName());
	}
	
	public void setPermission(PermissionDto permission) {
		this.permission = permission;
	}

	public void setRead(boolean read) {
		this.read = read;
	}

	public void setInsert(boolean insert) {
		this.insert = insert;
	}

	public void setUpdate(boolean update) {
		this.update = update;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}

	public PermissionDto getPermission() {
		return permission;
	}

	public ProfilePermissionDto(boolean read) {
		this.read = read;
	}

	public boolean isDisabledRead() {
		return disabledRead;
	}

	public boolean isDisabledInsert() {
		return disabledInsert;
	}

	public boolean isDisabledUpdate() {
		return disabledUpdate;
	}

	public boolean isDisabledDelete() {
		return disabledDelete;
	}

	public boolean isRead() {
		return read;
	}

	public boolean isInsert() {
		return insert;
	}

	public boolean isUpdate() {
		return update;
	}

	public boolean isDelete() {
		return delete;
	}
	
	public void setValuesCrud(boolean read, boolean insert, boolean update, boolean delete) {
		this.read = read;
		this.insert = insert;
		this.update = update;
		this.delete = delete;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((permission == null) ? 0 : permission.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProfilePermissionDto other = (ProfilePermissionDto) obj;
		if (permission == null) {
			if (other.permission != null)
				return false;
		} else if (permission.getId() != other.permission.getId())
			return false;
		return true;
	}

	public class PermissionDto {
		private int id;
		private String name;
		private boolean crud;

		public PermissionDto(int id, boolean crud, String name) {
			this.id = id;
			this.crud = crud;
			this.name = name;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public boolean isCrud() {
			return crud;
		}

		public void setCrud(boolean crud) {
			this.crud = crud;
		}
		
		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + id;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			PermissionDto other = (PermissionDto) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (id != other.id)
				return false;
			return true;
		}

		private ProfilePermissionDto getOuterType() {
			return ProfilePermissionDto.this;
		}

	}
}
