/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.web.validator;

import java.time.LocalDate;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.rrdevelopers.cac_nova_roma.common.security.util.LoggedUserUtil;
import com.rrdevelopers.cac_nova_roma.common.util.DateUtil;
import com.rrdevelopers.cac_nova_roma.common.validator.CACValidator;
import com.rrdevelopers.cac_nova_roma.control.model.Activity;
import com.rrdevelopers.cac_nova_roma.control.model.ActivityHistory;
import com.rrdevelopers.cac_nova_roma.control.model.ActivityType;
import com.rrdevelopers.cac_nova_roma.control.model.ActivityTypeCourse;
import com.rrdevelopers.cac_nova_roma.control.model.User;
import com.rrdevelopers.cac_nova_roma.control.model.enums.ActivityAppropiationEnum;
import com.rrdevelopers.cac_nova_roma.control.model.enums.ActivityStatusEnum;
import com.rrdevelopers.cac_nova_roma.control.model.enums.ProfileCategoryEnum;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IActivityFileService;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IActivityHistoryService;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IActivityService;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IActivityTypeCourseService;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IActivityTypeService;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IDisciplineService;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IUserService;
import com.rrdevelopers.cac_nova_roma.control.util.Util;

/**
 * @author rubens.ferreira
 *
 */
@Component
public class ActivityValidator extends CACValidator<Activity> {

	@Autowired
	private IActivityService activityService;

	@Autowired
	private IActivityHistoryService activityHistoryService;

	@Autowired
	private IActivityTypeService activityTypeService;

	@Autowired
	private IActivityFileService activityFileService;

	@Autowired
	private IActivityTypeCourseService activityTypeCourseService;

	@Autowired
	private IUserService userService;

	@Autowired
	private IDisciplineService disciplineService;

	@Override
	public boolean supports(Class<?> clazz) {
		return Activity.class.isAssignableFrom(clazz);
	}

	@Override
	public void validateEntity(Object object, Errors errors) {
		Activity activity = (Activity) object;

		if (!objectIsNull(activity, errors)) {

			// METODO PARA UPDATE
			if (activity.getId() > 0) {
				if (!objectNotExist(activityService.findById(activity.getId()), errors)) {
					activity.getActivityFiles().forEach(file -> {
						if (file.getId() > 0) {
							file.setActivity(activity);
							activityFileService.save(file);
						}
					});
				}
			}

			if (!isNullOrEmpty("name", activity.getName(), errors)) {
				checkSize("name", activity.getName(), 2, 1000, errors);
			}

			if (!objectIsNull("dateActivity", activity.getDateActivity(), errors)) {
				dateActivityIsInvalid(activity.getDateActivity(), errors);
			}

			if (!objectIsNull("student", activity.getStudent(), errors)) {

				// VERIFICACAO DO TIPO DO USUARIO
				if (!isStudentIsInvalid(activity.getStudent(), errors)) {

					// VERIFICACAO DE HORAS E INSERCAO DE ATIVIDADE
					if (!objectIsNull("activityType", activity.getActivityType(), errors)) {

						if (!isActivityTypeIsInvalid(activity.getActivityType(), errors)) {

							activity.setActivityType(activityTypeService.findById(activity.getActivityType().getId()));
							switch (activity.getActivityType().getCategory()) {
							case HOUR:
								if (!isNumberInvalid("numberHour", activity.getNumberHour(), errors)) {

									calculateHours(activity, errors);

								}
								break;
							case OCCURRENCE:
								if (!isNumberInvalid("numberOccurrence", activity.getNumberOccurrence(), errors)) {

									calculateHours(activity, errors);

								}
								break;
							}

							ActivityStatusEnum oldStatus = null;
							if (activity.getId() > 0) {
								oldStatus = activityService.findById(activity.getId()).getStatus();
							}

							// METODO PARA UPDATE
							if (activity.getId() > 0) {
								if (!objectNotExist(activityService.findById(activity.getId()), errors)) {

								}
							} else {
								// SÓ DEVE SER DEFINIDO O STATUS DE UMA ATIVIDADE QUANDO INSERIDO
								// MOTIVO: QUEM TEM PERMISSÃO DE ALTERAR DADOS POSSA ALTERAR SEM ALTERAR O
								// STATUS DELA PARA APROVADO/REJEITADO, COMO ADMIN OU SECRETARIA
								User loggedUser = LoggedUserUtil.getUser();
								if (loggedUser.getProfile().getType().getCategory() == ProfileCategoryEnum.STUDENT) {
									activity.setStatus(ActivityStatusEnum.PENDING);
								} else if (loggedUser.getProfile().getType()
										.getCategory() == ProfileCategoryEnum.SECRETARY
										|| loggedUser.getProfile().getType()
												.getCategory() == ProfileCategoryEnum.ADMIN) {
									activity.setStatus(ActivityStatusEnum.APPROVED);
								}
							}

							if (activity.getId() > 0) {
								ActivityStatusEnum currentStatus = activity.getStatus();
								if (!oldStatus.equals(currentStatus)) {
									activityHistoryService.save(new ActivityHistory(oldStatus.getText(),
											(currentStatus).getText(), activity));
								}
							}
						}
					}
				}
			}
		}
	}

	private void calculateHours(Activity activity, Errors errors) {

		activity.setStudent(userService.findById(activity.getStudent().getId()));

		// Total de horas disciplinas
		int totalHoursDiscipline = disciplineService.getSumHoursAllDisciplines();

		// Total de horas contabilizadas de atividades do aluno passado por parâmetro
		int totalHoursAccounted = activityService.getSumHoursAccountedByStudent(activity.getStudent().getId());

		// Relacionamento entre Tipo de Atividade e Curso
		ActivityTypeCourse activityTypeCourse = activityTypeCourseService.getActivityTypeCourse(
				activity.getActivityType().getId(), activity.getStudent().getCourse().getId(),
				Util.getCurrentSemester());

		if (!isActivityTypeCourseIsInvalid(activityTypeCourse, errors)) {

			// Dividir pela relação HORA para TIPO DE ATIVIDADE E CURSO, para todo o calculo
			// ser feito por cima de divisão

			// Inserir como GLOSA, porque o aluno já atingiu o limite de horas de atividade
			// complementar
			if (totalHoursAccounted >= totalHoursDiscipline) {

				switch (activity.getActivityType().getCategory()) {
				case HOUR:
					insertLikeGlosaHour(activity, activityTypeCourse);
					break;
				case OCCURRENCE:
					insertLikeGlosaOccurrence(activity, activityTypeCourse);
					break;
				}

			} else {

				if (!objectIsNull(activityTypeCourse, errors)) {
					switch (activity.getActivityType().getCategory()) {
					case HOUR:
						insertActivityTypeHour(activity, activityTypeCourse, totalHoursAccounted, totalHoursDiscipline,
								errors);
						break;
					case OCCURRENCE:
						insertActivityTypeOccurrence(activity, activityTypeCourse, totalHoursAccounted,
								totalHoursDiscipline, errors);
						break;
					}
				}

			}
		}
	}

	private void insertActivityTypeHour(Activity activity, ActivityTypeCourse activityTypeCourse, int totalSumActivity,
			int totalHoursDiscipline, Errors errors) {

		// Total de horas contabilizadas por atividade e por aluno passado por parâmetro
		int totalHoursTypeActivity = activityService.getSumHoursAccountedByStudentAndActivityType(
				activity.getStudent().getId(), activity.getActivityType().getId());

		int qtHourMax = activityTypeCourse.getNumberHourMax();

		if (!isEqualZero("hourMax", qtHourMax, errors)) {

			// Apropriação com GLOSA, porque o limite de horas para esse TIPO DE ATIVIDADE E
			// CURSO foi atingido ou ultrapassado
			if (totalHoursTypeActivity >= qtHourMax) {

				insertLikeGlosaHour(activity, activityTypeCourse);

			} else {

				// Divide pelo valor da Relação Hora cadastrada
				int totalHour = (totalHoursTypeActivity)
						+ (activity.getNumberHour() / activityTypeCourse.getNumberHourRelationship());

				if (!isEqualZero("totalHour", totalHour, errors)) {

					if (totalHour <= qtHourMax) { // Apropriacao Total

						activity.setTypeAppropiation(ActivityAppropiationEnum.TOTAL);
						activity.setNumberHourAccounted(
								activity.getNumberHour() / activityTypeCourse.getNumberHourRelationship());
						activity.setNumberHourGlosa(0);
						calculateGlosaDiscipline(activity, totalSumActivity, totalHoursDiscipline);

					} else { // Apropriacao Parcial

						// Total de horas parcial = totalDeHoras somadas menos o limite
						int totalHourPartial = qtHourMax - totalHoursTypeActivity;

						activity.setTypeAppropiation(ActivityAppropiationEnum.PARTIAL);
						activity.setNumberHourAccounted(qtHourMax - totalHoursTypeActivity);
						activity.setNumberHourGlosa(
								(activity.getNumberHour() / activityTypeCourse.getNumberHourRelationship())
										- totalHourPartial);
						calculateGlosaDiscipline(activity, totalSumActivity, totalHoursDiscipline);

					}
				}
			}
		}
	}

	private void insertActivityTypeOccurrence(Activity activity, ActivityTypeCourse activityTypeCourse,
			int totalSumActivity, int totalHoursDiscipline, Errors errors) {

		int totalOccurrenceByTypeActivity = activityService.getSumOccurenceAccountedByStudentAndActivityType(
				activity.getStudent().getId(), activity.getActivityType().getId());

		if (totalOccurrenceByTypeActivity >= activityTypeCourse.getNumberOccurrenceMax()) {

			// Inserir como GLOSA porque atingiu o maximo de ocorrencias
			insertLikeGlosaOccurrence(activity, activityTypeCourse);

		} else if ((totalOccurrenceByTypeActivity + activity.getNumberOccurrence()) > activityTypeCourse
				.getNumberOccurrenceMax()) {

			// Total de horas parcial
			int occurrencePartial = totalOccurrenceByTypeActivity - activityTypeCourse.getNumberOccurrenceMax();

			// Caso o resultado seja negativo multiplicar por -1 para geral saldo positivo
			occurrencePartial = (occurrencePartial < 0) ? occurrencePartial * (-1) : occurrencePartial;

			activity.setTypeAppropiation(ActivityAppropiationEnum.PARTIAL);

			activity.setNumberOccurrenceAccounted(occurrencePartial);
			activity.setNumberOccurrenceGlosa(activity.getNumberOccurrence() - occurrencePartial);

			activity.setNumberHourAccounted(
					activity.getNumberOccurrenceAccounted() * activityTypeCourse.getNumberHourByOccurrence());
			activity.setNumberHourGlosa(
					activity.getNumberOccurrenceGlosa() * activityTypeCourse.getNumberHourByOccurrence());
			calculateGlosaDiscipline(activity, totalSumActivity, totalHoursDiscipline);

		} else {

			int hourMax = activityTypeCourse.getNumberHourByOccurrence() * activityTypeCourse.getNumberOccurrenceMax();

			if (!isEqualZero("hourMax", hourMax, errors)) {

				int totalHour = totalOccurrenceByTypeActivity
						+ (activity.getNumberOccurrence() * activityTypeCourse.getNumberHourByOccurrence());

				if (!isEqualZero("totalHour", totalHour, errors)) {

					if (totalHour <= hourMax) { // Apropriacao Total

						activity.setTypeAppropiation(ActivityAppropiationEnum.TOTAL);
						activity.setNumberHourAccounted(
								activity.getNumberOccurrence() * activityTypeCourse.getNumberHourByOccurrence());
						activity.setNumberHourGlosa(0);
						activity.setNumberOccurrenceAccounted(activity.getNumberOccurrence());
						activity.setNumberOccurrenceGlosa(0);
						calculateGlosaDiscipline(activity, totalSumActivity, totalHoursDiscipline);

					} else { // Apropriacao Parcial

						// Total de horas parcial
						int occurrencePartial = totalOccurrenceByTypeActivity
								- activityTypeCourse.getNumberOccurrenceMax();

						// Caso o resultado seja negativo multiplicar por -1 para geral saldo positivo
						occurrencePartial = (occurrencePartial < 0) ? occurrencePartial * (-1) : occurrencePartial;

						activity.setTypeAppropiation(ActivityAppropiationEnum.PARTIAL);

						activity.setNumberOccurrenceAccounted(occurrencePartial);
						activity.setNumberOccurrenceGlosa(activity.getNumberOccurrence() - occurrencePartial);

						activity.setNumberHourAccounted(activity.getNumberOccurrenceAccounted()
								* activityTypeCourse.getNumberHourByOccurrence());
						activity.setNumberHourGlosa(
								activity.getNumberOccurrenceGlosa() * activityTypeCourse.getNumberHourByOccurrence());

						calculateGlosaDiscipline(activity, totalSumActivity, totalHoursDiscipline);

					}
				}
			}
		}
	}

	private void insertLikeGlosaHour(Activity activity, ActivityTypeCourse activityTypeCourse) {

		activity.setNumberHourAccounted(0);
		activity.setNumberHourGlosa(activity.getNumberHour() / activityTypeCourse.getNumberHourRelationship());
		activity.setTypeAppropiation(ActivityAppropiationEnum.GLOSA);

	}

	private void calculateGlosaDiscipline(Activity activity, int totalSumActivity, int totalSumDiscipline) {
		int nrHourTotal = (totalSumActivity + activity.getNumberHourAccounted());

		if (!activity.getTypeAppropiation().equals(ActivityAppropiationEnum.GLOSA)
				&& nrHourTotal > totalSumDiscipline) {
			int nrHourAccounted = nrHourTotal - totalSumDiscipline;
			activity.setNumberHourAccounted(activity.getNumberHourAccounted() - nrHourAccounted);
			activity.setNumberHourGlosa(activity.getNumberHourGlosa() + nrHourAccounted);
			activity.setTypeAppropiation(ActivityAppropiationEnum.PARTIAL);
		}

	}

	private void insertLikeGlosaOccurrence(Activity activity, ActivityTypeCourse activityTypeCourse) {

		int qtHour = activity.getNumberOccurrence() * activityTypeCourse.getNumberHourByOccurrence();

		activity.setNumberHourAccounted(0);
		activity.setNumberHourGlosa(qtHour);
		activity.setNumberOccurrenceAccounted(0);
		activity.setNumberOccurrenceGlosa(activity.getNumberOccurrence());
		activity.setTypeAppropiation(ActivityAppropiationEnum.GLOSA);

	}

	private boolean dateActivityIsInvalid(LocalDate date, Errors errors) {
		if (DateUtil.isAfterThanNow(date)) {
			errors.rejectValue("dateActivity", "activity.dateActivity.invalid", "The date must be before today");
			return true;
		}

		return false;
	}

	private boolean isActivityTypeIsInvalid(ActivityType activityType, Errors errors) {
		ActivityType activityTypeFound = activityTypeService.findById(activityType.getId());

		if (Objects.isNull(activityTypeFound)) {
			errors.rejectValue("activityType", "activity.activityType.invalidObject", "Object not found");
			return true;
		}

		return false;
	}

	private boolean isActivityTypeCourseIsInvalid(ActivityTypeCourse activityTypeCourse, Errors errors) {
		if (Objects.isNull(activityTypeCourse)) {
			errors.rejectValue("activityType", "activity.activityTypeCourse.semesterNotRegistered",
					"Semester not Registered");
			return true;
		}
		return false;
	}

	private boolean isStudentIsInvalid(User user, Errors errors) {
		User userFound = userService.findById(user.getId());
		if (Objects.isNull(userFound)) {
			errors.rejectValue("student", "activity.student.invalidObject", "User not found");
			return true;
		} else if (!userFound.getProfile().getType().getCategory().equals(ProfileCategoryEnum.STUDENT)) {
			errors.rejectValue("student", "activity.student.invalidType", "User is not a student");
			return true;
			// } else if
			// (!userFound.getProfile().getType().getCategory().equals(ProfileCategoryEnum.STUDENT)
			// && LoggedUserUtil.getUser().getId() == userFound.getId()) {
			// errors.rejectValue("student", "activity.user.invalidObject", "User is
			// invalid");
			// return true;
		}
		return false;
	}

}
