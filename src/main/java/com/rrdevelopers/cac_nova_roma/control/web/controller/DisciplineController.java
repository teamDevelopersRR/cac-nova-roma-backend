/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rrdevelopers.cac_nova_roma.common.handler.exceptions.GenericValidationException;
import com.rrdevelopers.cac_nova_roma.control.model.Discipline;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IDisciplineService;
import com.rrdevelopers.cac_nova_roma.control.web.validator.DisciplineValidator;

/**
 * @author Renatão
 *
 */
@RestController
@RequestMapping("/discipline")
@PreAuthorize("hasRole('ROLE_DISCIPLINE')")
public class DisciplineController {

	@Autowired
	private IDisciplineService service;

	@Autowired
	private DisciplineValidator validator;

	@InitBinder
	protected void initiBinder(WebDataBinder binder, HttpServletRequest request) {
		validator.setMethod(RequestMethod.valueOf(request.getMethod()));
		binder.addValidators(validator);
	}

	@PostMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_DISCIPLINE_INSERT')")
	public ResponseEntity<Discipline> save(@RequestBody @Valid Discipline discipline, BindingResult result,
			HttpServletResponse response) {
		if (result.hasErrors()) {
			throw new GenericValidationException(result, "Error to Save Discipline");
		}
		service.save(discipline);
		return new ResponseEntity<Discipline>(discipline, HttpStatus.OK);
	}

	@PutMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_DISCIPLINE_UPDATE')")
	public Discipline update(@RequestBody @Valid Discipline discipline, BindingResult result, HttpServletResponse response) {
		if (result.hasErrors()) {
			throw new GenericValidationException(result, "Error to Update Discipline");
		}
		service.save(discipline);
		return discipline;
	}

	@DeleteMapping(value = { "/{id}" })
	@PreAuthorize("hasRole('ROLE_DISCIPLINE_DELETE')")
	public boolean delete(@PathVariable("id") Integer id) {
		try{
			Discipline discipline = service.findById(id);
			return service.delete(discipline);
		} catch(Exception e){
			throw new GenericValidationException(null, "Error to Delete Discipline");
		}
	}

	@GetMapping("/{id}")
	@PreAuthorize("hasRole('ROLE_DISCIPLINE_READ')")
	public ResponseEntity<Discipline> findById(@PathVariable("id") Integer id) {
		return new ResponseEntity<Discipline>(service.findById(id), HttpStatus.OK);
	}

	@GetMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_DISCIPLINE_READ')")
	public ResponseEntity<List<Discipline>> findAllByOrderByNameAsc() {
		return new ResponseEntity<List<Discipline>>(service.findAllByOrderByNameAsc(), HttpStatus.OK);
	}

	@PostMapping(value = "/getByName")
	@PreAuthorize("hasRole('ROLE_DISCIPLINE_READ')")
	public ResponseEntity<List<Discipline>> getByNameContainsIgnoreCaseOrderByNameAsc(@RequestParam("name") String name) {
		return new ResponseEntity<List<Discipline>>(service.getByNameContainsIgnoreCaseOrderByNameAsc(name), HttpStatus.OK);
	}

}
