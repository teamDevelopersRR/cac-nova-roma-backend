/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rrdevelopers.cac_nova_roma.common.handler.exceptions.GenericValidationException;
import com.rrdevelopers.cac_nova_roma.control.model.Permission;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IPermissionService;
import com.rrdevelopers.cac_nova_roma.control.web.validator.PermissionValidator;

/**
 * @author Renatão
 *
 */
@RestController
@RequestMapping("/permission")
@PreAuthorize("hasRole('ROLE_PERMISSION')")
public class PermissionController {

	@Autowired
	private IPermissionService service;

	@Autowired
	private PermissionValidator validator;

	@InitBinder
	protected void initiBinder(WebDataBinder binder, HttpServletRequest request) {
		validator.setMethod(RequestMethod.valueOf(request.getMethod()));
		binder.addValidators(validator);
	}

	@PostMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_PERMISSION_INSERT')")
	public ResponseEntity<Permission> save(@RequestBody @Valid Permission permission, BindingResult result,
			HttpServletResponse response) {
		if (result.hasErrors()) {
			throw new GenericValidationException(result, "Error to Save Permission");
		}
		service.save(permission);
		return new ResponseEntity<Permission>(permission, HttpStatus.OK);
	}

	@PutMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_PERMISSION_UPDATE')")
	public Permission update(@RequestBody @Valid Permission permission, BindingResult result, HttpServletResponse response) {
		if (result.hasErrors()) {
			throw new GenericValidationException(result, "Error to Update Permission");
		}
		service.save(permission);
		return permission;
	}

	@DeleteMapping(value = { "/{id}" })
	@PreAuthorize("hasRole('ROLE_PERMISSION_DELETE')")
	public boolean delete(@PathVariable("id") Integer id) {
		try{
			Permission permission = service.findById(id);
			return service.delete(permission);
		} catch(Exception e){
			throw new GenericValidationException(null, "Error to Delete Permission");
		}
	}

	@GetMapping("/{id}")
	@PreAuthorize("hasRole('ROLE_PERMISSION_READ')")
	public ResponseEntity<Permission> findById(@PathVariable("id") Integer id) {
		return new ResponseEntity<Permission>(service.findById(id), HttpStatus.OK);
	}

	@GetMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_PERMISSION_READ')")
	public ResponseEntity<List<Permission>> findAllByOrderByNameAsc() {
		return new ResponseEntity<List<Permission>>(service.findAllByOrderByNameAsc(), HttpStatus.OK);
	}

	@PostMapping(value = "/getByName")
	@PreAuthorize("hasRole('ROLE_PERMISSION_READ')")
	public ResponseEntity<List<Permission>> getByNameContainsIgnoreCaseOrderByNameAsc(@RequestParam("name") String name) {
		return new ResponseEntity<List<Permission>>(service.getByNameContainsIgnoreCaseOrderByNameAsc(name), HttpStatus.OK);
	}

}
