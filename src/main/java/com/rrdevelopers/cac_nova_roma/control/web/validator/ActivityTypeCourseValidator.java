/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.web.validator;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.rrdevelopers.cac_nova_roma.common.validator.CACValidator;
import com.rrdevelopers.cac_nova_roma.control.model.ActivityTypeCourse;
import com.rrdevelopers.cac_nova_roma.control.model.enums.ActivityTypeCategoryEnum;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IActivityTypeCourseService;

/**
 * @author rubens.ferreira
 *
 */
@Component
public class ActivityTypeCourseValidator extends CACValidator<ActivityTypeCourse> {

	@Autowired
	private IActivityTypeCourseService service;

	@Override
	public boolean supports(Class<?> clazz) {
		return ActivityTypeCourse.class.isAssignableFrom(clazz);
	}

	@Override
	public void validateEntity(Object object, Errors errors) {
		ActivityTypeCourse activityTypeCourse = (ActivityTypeCourse) object;

		if (!objectIsNull(activityTypeCourse, errors)) {

			// METODO PARA UPDATE
			if (activityTypeCourse.getId() > 0) {
				if (!objectNotExist(service.findById(activityTypeCourse.getId()), errors)) {

				}
			} else {
				List<ActivityTypeCourse> list = service.getActivityTypeCourseByActivityTypeAndCourse(activityTypeCourse.getActivityType().getId(), activityTypeCourse.getCourse().getId());
				checkUniqueRegister("course", list, errors);
			}

			isNumberInvalid("activityType", activityTypeCourse.getActivityType().getId(), errors);
			objectIsNull("activityType", activityTypeCourse.getActivityType(), errors);
			
			isNumberInvalid("course", activityTypeCourse.getCourse().getId(), errors);
			objectIsNull("course", activityTypeCourse.getCourse(), errors);
			
			if(activityTypeCourse.getActivityType().getCategory() == ActivityTypeCategoryEnum.HOUR){
				
				isNumberInvalid("numberHourMax", activityTypeCourse.getNumberHourMax(), errors);
				isNumberInvalid("numberHourRelationship", activityTypeCourse.getNumberHourRelationship(), errors);
				
			} else if(activityTypeCourse.getActivityType().getCategory() == ActivityTypeCategoryEnum.OCCURRENCE){
				
				isNumberInvalid("numberHourByOccurrence", activityTypeCourse.getNumberHourByOccurrence(), errors);
				isNumberInvalid("numberOccurrenceMax", activityTypeCourse.getNumberOccurrenceMax(), errors);
				
			}

		}

	}

}
