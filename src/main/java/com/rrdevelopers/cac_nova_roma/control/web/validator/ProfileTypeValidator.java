/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.web.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.rrdevelopers.cac_nova_roma.common.validator.CACValidator;
import com.rrdevelopers.cac_nova_roma.control.model.ProfileType;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IProfileTypeService;

/**
 * @author Renatão
 *
 */
@Component
public class ProfileTypeValidator extends CACValidator<ProfileType> {

	@Autowired
	private IProfileTypeService service;

	@Override
	public boolean supports(Class<?> clazz) {
		return ProfileType.class.isAssignableFrom(clazz);
	}

	@Override
	public void validateEntity(Object object, Errors errors) {
		ProfileType profileType = (ProfileType) object;

		if (!objectIsNull(profileType, errors)) {

			// METODO PARA UPDATE
			if (profileType.getId() > 0) {
				if (!objectNotExist(service.findById(profileType.getId()), errors)) {

				}
			}
			
			if (!isNullOrEmpty("name", profileType.getName(), errors)) {
				checkSize("name", profileType.getName(), 1, 100, errors);
			}
			
			if (!isNullOrEmpty("category", profileType.getCategory().getText(), errors)) {
				checkProfileCategoryEnum("category", profileType.getCategory().getText(), errors);
			}
			
			if (!isNullOrEmpty("description", profileType.getDescription(), errors)) {
				checkSize("description", profileType.getDescription(), 1, 1000, errors);
			}
			
		}
	}

}
