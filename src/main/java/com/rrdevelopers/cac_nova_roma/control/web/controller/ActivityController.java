/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.web.controller;

import java.util.List;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.rrdevelopers.cac_nova_roma.common.handler.exceptions.GenericValidationException;
import com.rrdevelopers.cac_nova_roma.control.model.Activity;
import com.rrdevelopers.cac_nova_roma.control.model.ActivityHistory;
import com.rrdevelopers.cac_nova_roma.control.model.enums.ActivityStatusEnum;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IActivityHistoryService;
import com.rrdevelopers.cac_nova_roma.control.service.interfaces.IActivityService;
import com.rrdevelopers.cac_nova_roma.control.web.validator.ActivityValidator;

/**
 * @author rubens.ferreira
 *
 */
@RestController
@RequestMapping("/activity")
@PreAuthorize("hasRole('ROLE_ACTIVITY')")
public class ActivityController {

	@Autowired
	private IActivityService service;

	@Autowired
	private IActivityHistoryService activityHistoryService;

	@Autowired
	private ActivityValidator validator;

	@InitBinder
	protected void initiBinder(WebDataBinder binder, HttpServletRequest request) {
		validator.setMethod(RequestMethod.valueOf(request.getMethod()));
		binder.addValidators(validator);
	}

	@PostMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_ACTIVITY_INSERT')")
	public ResponseEntity<Activity> save(@RequestBody @Valid Activity activity, BindingResult result,
			HttpServletResponse response) {
		if (result.hasErrors()) {
			throw new GenericValidationException(result, "Error to Save Activity");
		}
		ActivityStatusEnum currentStatus = activity.getStatus();
		activityHistoryService.save(new ActivityHistory(ActivityStatusEnum.CREATED.getText(), (currentStatus).getText(), activity));
		return new ResponseEntity<Activity>(activity, HttpStatus.OK);
	}

	@PutMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_ACTIVITY_UPDATE')")
	public Activity update(@RequestBody @Valid Activity activity, BindingResult result, HttpServletResponse response) {
		if (result.hasErrors()) {
			throw new GenericValidationException(result, "Error to Update Activity");
		}

		service.save(activity);
		return activity;
	}

	@GetMapping("/{id}")
	@PreAuthorize("hasRole('ROLE_ACTIVITY_READ')")
	public ResponseEntity<Activity> findById(@PathVariable("id") Integer id) {
		return new ResponseEntity<Activity>(service.findById(id), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	@PreAuthorize("hasRole('ROLE_ACTIVITY_DELETE')")
	public ResponseEntity<Activity> deleteLogic(@PathVariable("id") Integer id) {

		Activity activity = service.findById(id);
		activity.setActive(false);

		service.save(activity);

		return new ResponseEntity<Activity>(service.findById(id), HttpStatus.OK);
	}

	@GetMapping(value = { "/", "" })
	@PreAuthorize("hasRole('ROLE_ACTIVITY_READ')")
	public ResponseEntity<List<Activity>> findAllByActiveTrueOrderByDateActivityAsc() {
		return new ResponseEntity<List<Activity>>(service.findAllByActiveTrueOrderByDateActivityAsc(), HttpStatus.OK);
	}

	@PostMapping(value = "/file")
	@PreAuthorize("hasRole('ROLE_ACTIVITY_READ')")
	public String getFile(@RequestParam("file") MultipartFile file) {
		return "deu";
	}

	@GetMapping("/findByStudentId/{studentId}")
	@PreAuthorize("hasRole('ROLE_ACTIVITY_READ')")
	public ResponseEntity<List<Activity>> findByStudentId(@PathVariable("studentId") Integer studentId) {
		return new ResponseEntity<List<Activity>>(service.findByStudentIdAndActiveTrueOrderByDateActivityAsc(studentId),
				HttpStatus.OK);
	}

	@PostMapping(value = "/approveOrReject/{activityId}/{approve}")
	@PreAuthorize("hasRole('ROLE_ACTIVITY_UPDATE')")
	public ResponseEntity<Activity> approveActivity(@PathVariable("activityId") Integer activityId,
			@PathVariable("approve") boolean approve) {
		
		Activity activity = service.findById(activityId);
		ActivityStatusEnum oldStatus = service.findById(activity.getId()).getStatus();

		if (activity.getId() > 0 && !(Objects.isNull(activity))) {
			activity.setStatus(approve ? ActivityStatusEnum.APPROVED : ActivityStatusEnum.REJECTED);
			activityHistoryService.save(new ActivityHistory(oldStatus.getText(), activity.getStatus().getText(), activity));
			return new ResponseEntity<Activity>(service.save(activity), HttpStatus.OK);
		}
		return new ResponseEntity<Activity>(activity, HttpStatus.PRECONDITION_FAILED);
	}
	
	@GetMapping(value = "activityHistory/{activityId}")
	@PreAuthorize("hasRole('ROLE_ACTIVITY_READ')")
	public ResponseEntity<List<ActivityHistory>> findActiviyHistory(@PathVariable("activityId") Integer activityId) {
		return new ResponseEntity<List<ActivityHistory>>(activityHistoryService.findByActivityId(activityId), HttpStatus.OK);
	}

}
