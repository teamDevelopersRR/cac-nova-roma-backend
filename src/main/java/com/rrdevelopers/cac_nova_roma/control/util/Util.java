package com.rrdevelopers.cac_nova_roma.control.util;

import java.time.LocalDate;
import java.time.Month;

/**
 * @author Renatão
 *
 */
public class Util {
	
	public static String getCurrentSemester(){
		
		LocalDate currentDate = LocalDate.now();
		
		int year = currentDate.getYear();
		
		int semester = currentDate.getMonthValue() < Month.JULY.getValue() ? 1:2;
		
		return year + "." + semester;
	}
	
}
