package com.rrdevelopers.cac_nova_roma.control.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.rrdevelopers.cac_nova_roma.control.model.Activity;

/**
 * @author rubens_ferreira
 *
 */
public interface IActivityDAO extends JpaRepository<Activity, Serializable> {

	Activity findById(Integer idActivity);

	List<Activity> findByName(String name);
	
	List<Activity> findAllByActiveTrueOrderByDateActivityAsc();

	@Query(value = "SELECT IFNULL(SUM(nr_hour_accounted), 0) nr_hour_accounted, nr_hour_relationship " +
					" FROM activity, activity_type, activity_type_course, course, users " +
					"WHERE activity.id_activity_type = activity_type.id_activity_type " +
					"  AND activity_type_course.id_activity_type = activity_type.id_activity_type " +
					"  AND activity_type_course.id_course = users.id_course " +
					"  AND activity.id_user = users.id_user " +
					"  AND users.id_course = course.id_course " +
					"  AND users.id_user =:userId " +
					"  AND activity.ck_active = 1 " +
					"  AND activity_type.id_activity_type =:activityTypeId " +
					"  AND tp_appropiation != 'GLOSA'", nativeQuery = true)
	Integer getSumHoursAccountedByStudentAndActivityType(@Param("userId") Integer userId, @Param("activityTypeId") Integer activityTypeId);
	
	@Query(value = " SELECT IFNULL(SUM(nr_hour_accounted), 0) " + 
					"  FROM activity " + 
					" WHERE activity.id_user =:userId " +
					"   AND activity.ck_active = 1 ", nativeQuery = true)
	Integer getSumHoursAccountedByStudent(@Param("userId") Integer userId);
	
	@Query(value = " SELECT IFNULL(SUM(nr_occurrence_accounted),0) " +
					" FROM activity " +
					"WHERE id_activity_type =:activityTypeId " +
					"  AND id_user =:userId", nativeQuery = true)
	Integer getSumOccurenceAccountedByStudentAndActivityType(@Param("userId") Integer userId, @Param("activityTypeId") Integer activityTypeId);
	
	List<Activity> findByStudentIdAndActiveTrueOrderByDateActivityAsc(Integer studentId);
	
	List<Activity> findByStudentId(Integer studentId);
	
}
