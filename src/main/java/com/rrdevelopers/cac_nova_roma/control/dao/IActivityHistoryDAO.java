package com.rrdevelopers.cac_nova_roma.control.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rrdevelopers.cac_nova_roma.control.model.ActivityHistory;

/**
 * @author Renatão
 *
 */
public interface IActivityHistoryDAO extends JpaRepository<ActivityHistory, Serializable> { 
	
	List<ActivityHistory> findByActivityId(Integer activityId);

}
