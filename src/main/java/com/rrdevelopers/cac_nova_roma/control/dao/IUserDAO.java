/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rrdevelopers.cac_nova_roma.control.model.User;
import com.rrdevelopers.cac_nova_roma.control.model.enums.ProfileCategoryEnum;

/**
 * @author rubens.ferreira
 *
 */
public interface IUserDAO extends JpaRepository<User, Serializable> {

	User findByLoginAndPassword(String login, String password);
	
	User findByLogin(String login);
	
	List<User> findByLoginContainsIgnoreCaseAndProfileProfileTypeCategory(String login, ProfileCategoryEnum type);
	
	List<User> findAllByOrderByNameAsc();
	
}
