package com.rrdevelopers.cac_nova_roma.control.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rrdevelopers.cac_nova_roma.control.model.ActivityType;

/**
 * @author Renatão
 *
 */
public interface IActivityTypeDAO extends JpaRepository<ActivityType, Serializable> {
	
	ActivityType findByName(String name);
	
	List<ActivityType> findAllByOrderByNameAsc();
	
}
