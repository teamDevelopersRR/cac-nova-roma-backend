package com.rrdevelopers.cac_nova_roma.control.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.rrdevelopers.cac_nova_roma.control.model.Discipline;

/**
 * @author Renatão
 *
 */
public interface IDisciplineDAO extends JpaRepository<Discipline, Serializable> {

	@Query(value = "SELECT SUM(nr_hour) FROM discipline", nativeQuery = true)
	int getSumHoursAllDisciplines();

	List<Discipline> getByNameContainsIgnoreCaseOrderByNameAsc(String name);
	
	List<Discipline> findAllByOrderByNameAsc();
	
}
