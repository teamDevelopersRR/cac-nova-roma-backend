package com.rrdevelopers.cac_nova_roma.control.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.rrdevelopers.cac_nova_roma.control.model.CourseDiscipline;

/**
 * @author Renatão
 *
 */
public interface ICourseDisciplineDAO extends JpaRepository<CourseDiscipline, Serializable> {

	@Query(value = " SELECT course_discipline.* " + 
					"  FROM course_discipline, course " + 
					" WHERE course_discipline.id_course = course.id_course " +
					"   AND course_discipline.id_course =:courseId " + 
					" ORDER BY course.nm_course", nativeQuery = true)
	List<CourseDiscipline> getByCourse(@Param("courseId") Integer courseId);

	@Query(value = "SELECT * FROM course_discipline WHERE id_course =:idCourse AND id_discipline =:idDiscipline ", nativeQuery = true)
	List<CourseDiscipline> getCourseDisciplineByCourseAndDiscipline(@Param("idCourse") Integer idCourse, @Param("idDiscipline") Integer idDiscipline);

}
