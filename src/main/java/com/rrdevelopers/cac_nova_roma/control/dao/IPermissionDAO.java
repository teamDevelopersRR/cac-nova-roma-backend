package com.rrdevelopers.cac_nova_roma.control.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rrdevelopers.cac_nova_roma.control.model.Permission;

/**
 * @author Renatão
 *
 */
public interface IPermissionDAO extends JpaRepository<Permission, Serializable> {

	List<Permission> getByNameContainsIgnoreCaseOrderByNameAsc(String name);
	
	List<Permission> findAllByOrderByNameAsc();
	
	Permission findByName(String name);

}
