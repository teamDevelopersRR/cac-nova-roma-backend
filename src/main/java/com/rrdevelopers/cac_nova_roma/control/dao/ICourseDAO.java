package com.rrdevelopers.cac_nova_roma.control.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rrdevelopers.cac_nova_roma.control.model.Course;

/**
 * @author RenatÃ£o
 *
 */
public interface ICourseDAO extends JpaRepository<Course, Serializable> {

	List<Course> getByNameContainsIgnoreCaseOrderByNameAsc(String name);
	
	List<Course> getByCodeIgnoreCaseOrderByNameAsc(String code);
	
	List<Course> findAllByOrderByNameAsc();

}
