/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.dao;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rrdevelopers.cac_nova_roma.control.model.UserTrace;

/**
 * @author Renatão
 *
 */
public interface IUserTraceDAO extends JpaRepository<UserTrace, Serializable> {

}
