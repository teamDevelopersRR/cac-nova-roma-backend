package com.rrdevelopers.cac_nova_roma.control.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rrdevelopers.cac_nova_roma.control.model.ActivityFile;

/**
 * @author rubens.ferreia
 *
 */
public interface IActivityFileDAO extends JpaRepository<ActivityFile, Serializable> {
	
	List<ActivityFile> findByActivityId(long idActivity);
	
	List<ActivityFile> findAllByOrderByNameAsc();
	
}
