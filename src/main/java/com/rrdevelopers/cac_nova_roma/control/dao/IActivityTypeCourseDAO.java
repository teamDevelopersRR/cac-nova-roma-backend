package com.rrdevelopers.cac_nova_roma.control.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.rrdevelopers.cac_nova_roma.control.model.ActivityTypeCourse;

/**
 * @author Renatão
 *
 */
public interface IActivityTypeCourseDAO extends JpaRepository<ActivityTypeCourse, Serializable> {
	
	@Query(value = " SELECT * " + 
					"  FROM activity_type_course " + 
					" WHERE id_activity_type =:activeTypeId " + 
					"   AND id_course =:courseId " +
					"   AND (ds_semester =:semesterDs OR ds_semester IS NULL OR ds_semester = '')", nativeQuery = true)
	ActivityTypeCourse getActivityTypeCourse(@Param("activeTypeId") int activityTypeId,
											 @Param("courseId") int courseId,
											 @Param("semesterDs") String semesterDs);
	
	@Query(value = " SELECT activity_type_course.* " + 
					"  FROM activity_type_course, course " + 
					" WHERE activity_type_course.id_course = course.id_course " +
					"   AND activity_type_course.id_activity_type =:activeTypeId " + 
					" ORDER BY course.nm_course", nativeQuery = true)
	List<ActivityTypeCourse> getByActivityType(@Param("activeTypeId") int activityTypeId);

	@Query(value = "SELECT * FROM activity_type_course WHERE id_activity_type =:idActivityType AND id_course =:idCourse ", nativeQuery = true)
	List<ActivityTypeCourse> getActivityTypeCourseByActivityTypeAndCourse(@Param("idActivityType") Integer idActivityType, @Param("idCourse") Integer idCourse);

}
