package com.rrdevelopers.cac_nova_roma.control.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rrdevelopers.cac_nova_roma.control.model.Profile;

/**
 * @author Renatão
 *
 */
public interface IProfileDAO extends JpaRepository<Profile, Serializable> {

	List<Profile> getByNameContainsIgnoreCase(String name);
	
	List<Profile> findAllByOrderByNameAsc();

}
