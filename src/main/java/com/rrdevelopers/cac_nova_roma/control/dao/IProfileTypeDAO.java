package com.rrdevelopers.cac_nova_roma.control.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rrdevelopers.cac_nova_roma.control.model.ProfileType;

/**
 * @author Renatão
 *
 */
public interface IProfileTypeDAO extends JpaRepository<ProfileType, Serializable> {
	
	List<ProfileType> findAllByOrderByNameAsc();

}
