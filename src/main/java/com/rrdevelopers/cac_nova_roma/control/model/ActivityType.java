package com.rrdevelopers.cac_nova_roma.control.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.rrdevelopers.cac_nova_roma.control.model.enums.ActivityTypeCategoryEnum;

/**
 * @author Renatão
 *
 */
@Entity
@SequenceGenerator(initialValue = 1, name = "base_gen", sequenceName = "activity_type_seq")
@Table(name = "activity_type")
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_activity_type")) })
public class ActivityType extends GenericEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "ds_activity_type", nullable = true, length = 1000)
	private String description;

	@Column(name = "nm_activity_type", nullable = false, length = 1000, unique = true)
	private String name;

	@Enumerated(EnumType.STRING)
	@Column(name = "tp_category", nullable = false)
	private ActivityTypeCategoryEnum category;

	public ActivityType() { }
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ActivityTypeCategoryEnum getCategory() {
		return category;
	}

	public void setCategory(ActivityTypeCategoryEnum category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
