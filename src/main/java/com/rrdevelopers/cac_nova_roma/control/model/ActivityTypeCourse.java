package com.rrdevelopers.cac_nova_roma.control.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * @author Renatão
 *
 */
@Entity
@SequenceGenerator(initialValue = 1, name = "base_gen", sequenceName = "activity_type_course_seq")
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_activity_type_course")) })
@Table(name="activity_type_course", uniqueConstraints = {@UniqueConstraint(columnNames = { "id_course", "id_activity_type" }) })
public class ActivityTypeCourse extends GenericEntity {
	
	private static final long serialVersionUID = 1L;
	
	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name = "id_activity_type", nullable = false)
	private ActivityType activityType;
	
	@ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name = "id_course", nullable = false)
	private Course course;
	
	@Column(name = "ds_semester", nullable = true, length = 6)
	private String semester;
	
	@Column(name = "nr_hour_by_occurrence", nullable = true)
	private Integer numberHourByOccurrence;
	
	@Column(name = "nr_occurrence_max", nullable = true)
	private Integer numberOccurrenceMax;
	
	@Column(name = "nr_hour_max", nullable = true)
	private Integer numberHourMax;
	
	@Column(name = "nr_hour_relationship", nullable = true)
	private Integer numberHourRelationship;
	
	public ActivityTypeCourse() { }
	
	public ActivityType getActivityType() {
		return activityType;
	}
	
	public void setActivityType(ActivityType activityType) {
		this.activityType = activityType;
	}
	
	public Course getCourse() {
		return course;
	}
	
	public void setCourse(Course course) {
		this.course = course;
	}
	
	public String getSemester() {
		return semester;
	}
	
	public void setSemester(String semester) {
		this.semester = semester;
	}
	
	public Integer getNumberHourByOccurrence() {
		return numberHourByOccurrence;
	}
	
	public void setNumberHourByOccurrence(Integer numberHourByOccurrence) {
		this.numberHourByOccurrence = numberHourByOccurrence;
	}
	
	public Integer getNumberHourMax() {
		return numberHourMax;
	}
	
	public void setNumberHourMax(Integer numberHourMax) {
		this.numberHourMax = numberHourMax;
	}
	
	public Integer getNumberOccurrenceMax() {
		return numberOccurrenceMax;
	}
	
	public void setNumberOccurrenceMax(Integer numberOccurrenceMax) {
		this.numberOccurrenceMax = numberOccurrenceMax;
	}

	public Integer getNumberHourRelationship() {
		return numberHourRelationship;
	}

	public void setNumberHourRelationship(Integer numberHourRelationship) {
		this.numberHourRelationship = numberHourRelationship;
	}
	
}
