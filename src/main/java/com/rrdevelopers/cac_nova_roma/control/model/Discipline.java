package com.rrdevelopers.cac_nova_roma.control.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author Renatão
 *
 */
@Entity
@SequenceGenerator(initialValue = 1, name = "base_gen", sequenceName = "discipline_seq")
@Table(name="discipline")
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_discipline")) })
public class Discipline extends GenericEntity {
	
	private static final long serialVersionUID = 1L;

	@Column(name = "nm_discipline", nullable = false, length = 100)
	private String name;
	
	@Column(name = "nr_hour", nullable = false)
	private Integer numberHour;
	
	public Discipline() { }
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Integer getNumberHour() {
		return numberHour;
	}
	
	public void setNumberHour(Integer numberHour) {
		this.numberHour = numberHour;
	}
	
}
