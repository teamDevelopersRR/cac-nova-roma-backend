package com.rrdevelopers.cac_nova_roma.control.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.rrdevelopers.cac_nova_roma.common.config.data.serializers.UserInsertUpdateSerializer;

/**
 * @author Renatão
 *
 */
@Entity
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_user_trace")) })
@SequenceGenerator(initialValue = 1, name = "base_gen", sequenceName = "user_trace_seq")
@Table(name = "user_trace")
public class UserTrace extends GenericEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "nm_action", nullable = false, length = 100)
	private String action;

	@Column(name = "ds_ip", nullable = false, length = 50)
	private String ip;
	
	@ManyToOne
	@JoinColumn(name = "id_user")
	@JsonSerialize(using = UserInsertUpdateSerializer.class)
	private User userInsert;
	
	public UserTrace() {
		
	}
	
	public UserTrace(String action, String ip, User userInsert) {
		this.action = action;
		this.ip = ip;
		this.userInsert = userInsert;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public User getUserInsert() {
		return userInsert;
	}

	public void setUserInsert(User userInsert) {
		this.userInsert = userInsert;
	}

}