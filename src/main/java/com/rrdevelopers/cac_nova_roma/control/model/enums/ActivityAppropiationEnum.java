/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.model.enums;

/**
 * @author rubens.ferreira
 *
 */
public enum ActivityAppropiationEnum implements IEnumDeserializer<ActivityAppropiationEnum> {
	
	PARTIAL("PARTIAL"),
	TOTAL("TOTAL"),
	GLOSA("GLOSA");

	private final String text;

	private ActivityAppropiationEnum(String text) {
		this.text = text;
	}

	public ActivityAppropiationEnum getTextEnumByString(String text) {
		for (ActivityAppropiationEnum activityAppropiationEnum : values())
			if (activityAppropiationEnum.text.equals(text))
				return activityAppropiationEnum;
		return null;
	}

	public String getText() {
		return text;
	}
}
