/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.model;

/**
 * @author rubens.ferreira
 *
 */
public interface Authenticated {
	String getLogin();

	String getPassword();

	void setPassword(String password);
}
