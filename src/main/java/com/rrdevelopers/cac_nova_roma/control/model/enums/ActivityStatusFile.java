/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.model.enums;

/**
 * @author rubens.ferreira
 *
 */
public enum ActivityStatusFile implements IEnumDeserializer<ActivityStatusFile> {
	
	ACTIVE("ACTIVE"),
	DELETED("DELETED"),
	NOT_LOADED("NOT_LOADED");

	private final String text;

	private ActivityStatusFile(String text) {
		this.text = text;
	}

	public ActivityStatusFile getTextEnumByString(String text) {
		for (ActivityStatusFile activityAppropiationEnum : values())
			if (activityAppropiationEnum.text.equals(text))
				return activityAppropiationEnum;
		return null;
	}

	public String getText() {
		return text;
	}
}
