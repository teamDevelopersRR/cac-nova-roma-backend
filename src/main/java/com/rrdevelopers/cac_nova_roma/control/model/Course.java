package com.rrdevelopers.cac_nova_roma.control.model;


import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author Renatão
 *
 */
@Entity
@SequenceGenerator(initialValue = 1, name = "base_gen", sequenceName = "course_seq")
@Table(name = "course")
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_course")) })
public class Course extends GenericEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "ds_code", nullable = false, length = 10, unique = true)
	private String code;

	@Column(name = "nm_course", nullable = false, length = 1000)
	private String name;

	@Column(name = "nr_hour_max", nullable = false)
	private Integer numberHourMax;

	public Course() { }

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNumberHourMax() {
		return numberHourMax;
	}

	public void setNumberHourMax(Integer numberHourMax) {
		this.numberHourMax = numberHourMax;
	}

}
