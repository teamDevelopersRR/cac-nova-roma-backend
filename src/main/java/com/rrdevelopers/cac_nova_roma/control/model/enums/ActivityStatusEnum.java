/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.model.enums;

/**
 * @author rubens.ferreira
 *
 */
public enum ActivityStatusEnum implements IEnumDeserializer<ActivityStatusEnum>{
	
	PENDING("PENDING"),
	APPROVED("APPROVED"),
	CREATED("CREATED"),
	REJECTED("REJECTED");

	private final String text;

	private ActivityStatusEnum(String text) {
		this.text = text;
	}

	@Override
	public ActivityStatusEnum getTextEnumByString(String text) {
		for (ActivityStatusEnum activityStatusEnum : values())
			if (activityStatusEnum.text.equals(text))
				return activityStatusEnum;
		return null;
	}

	public String getText() {
		return text;
	}
}
