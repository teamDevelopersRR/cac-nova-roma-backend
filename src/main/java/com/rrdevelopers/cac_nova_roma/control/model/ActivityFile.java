package com.rrdevelopers.cac_nova_roma.control.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.rrdevelopers.cac_nova_roma.control.model.enums.ActivityStatusFile;

/**
 * @author Renatão
 *
 */
@Entity
@SequenceGenerator(initialValue = 1, name = "base_gen", sequenceName = "activity_file_seq")
@Table(name = "activity_file")
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_activity_file")) })
public class ActivityFile extends GenericEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "nm_file", nullable = false, length = 200)
	private String name;

	@Column(name = "tp_file", nullable = false, length = 200)
	private String type;

	@Enumerated(EnumType.STRING)
	@Column(name = "nm_status", nullable = true)
	private ActivityStatusFile status;

	@Lob
	@Column(name = "byte_file", nullable = false, length = 2000)
	private byte[] file;

	@Column(name = "nr_size", nullable = false)
	private double size;

	@ManyToOne
	@JoinColumn(name = "id_activity")
	private Activity activity;

	public ActivityFile() {
	}

	public void setSize(double size) {
		this.size = size;
	}

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ActivityStatusFile getStatus() {
		return status;
	}

	public void setStatus(ActivityStatusFile status) {
		this.status = status;
	}

	public byte[] getFile() {
		return file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}

	public double getSize() {
		return size;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
