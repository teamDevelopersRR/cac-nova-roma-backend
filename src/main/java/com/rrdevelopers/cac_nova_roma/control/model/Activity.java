package com.rrdevelopers.cac_nova_roma.control.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.rrdevelopers.cac_nova_roma.common.config.data.serializers.ActivityFileListSerializer;
import com.rrdevelopers.cac_nova_roma.common.config.data.serializers.LocalDateDeserializer;
import com.rrdevelopers.cac_nova_roma.common.config.data.serializers.LocalDateSerializer;
import com.rrdevelopers.cac_nova_roma.control.model.enums.ActivityAppropiationEnum;
import com.rrdevelopers.cac_nova_roma.control.model.enums.ActivityStatusEnum;

/**
 * @author Renatão
 *
 */
@Entity
@SequenceGenerator(initialValue = 1, name = "base_gen", sequenceName = "activity_seq")
@Table(name = "activity")
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_activity")) })
public class Activity extends GenericEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "nm_activity", nullable = false, length = 1000)
	private String name;

	@Enumerated(EnumType.STRING)
	@Column(name = "tp_status", nullable = false, length = 50)
	private ActivityStatusEnum status;

	@Column(name = "dt_activity")
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	private LocalDate dateActivity;

	@Column(name = "ck_active")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean active = true;

	@Column(name = "nr_occurrence", nullable = true)
	private Integer numberOccurrence;

	@Column(name = "nr_occurrence_accounted", nullable = true)
	private Integer numberOccurrenceAccounted;

	@Column(name = "nr_occurrence_glosa", nullable = true)
	private Integer numberOccurrenceGlosa;

	@Column(name = "nr_hour", nullable = true)
	private Integer numberHour;

	@Column(name = "nr_hour_accounted", nullable = true)
	private Integer numberHourAccounted;

	@Column(name = "nr_hour_glosa", nullable = true)
	private Integer numberHourGlosa;

	@Enumerated(EnumType.STRING)
	@Column(name = "tp_appropiation", nullable = true)
	private ActivityAppropiationEnum typeAppropiation;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_user", nullable = false)
	private User student;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_activity_type", nullable = false)
	private ActivityType activityType;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "id_activity")
	@JsonSerialize(using = ActivityFileListSerializer.class)
	private List<ActivityFile> activityFiles;
	
	public Activity() { }
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ActivityStatusEnum getStatus() {
		return status;
	}

	public void setStatus(ActivityStatusEnum status) {
		this.status = status;
	}

	public LocalDate getDateActivity() {
		return dateActivity;
	}

	public void setDateActivity(LocalDate dateActivity) {
		this.dateActivity = dateActivity;
	}

	public Integer getNumberOccurrence() {
		return numberOccurrence;
	}

	public void setNumberOccurrence(Integer numberOccurrence) {
		this.numberOccurrence = numberOccurrence;
	}

	public Integer getNumberOccurrenceAccounted() {
		return numberOccurrenceAccounted;
	}

	public void setNumberOccurrenceAccounted(Integer numberOccurrenceAccounted) {
		this.numberOccurrenceAccounted = numberOccurrenceAccounted;
	}

	public Integer getNumberHour() {
		return numberHour;
	}

	public void setNumberHour(Integer numberHour) {
		this.numberHour = numberHour;
	}

	public Integer getNumberHourAccounted() {
		return numberHourAccounted;
	}

	public void setNumberHourAccounted(Integer numberHourAccounted) {
		this.numberHourAccounted = numberHourAccounted;
	}

	public ActivityAppropiationEnum getTypeAppropiation() {
		return typeAppropiation;
	}

	public void setTypeAppropiation(ActivityAppropiationEnum typeAppropiation) {
		this.typeAppropiation = typeAppropiation;
	}

	public User getStudent() {
		return student;
	}

	public void setStudent(User student) {
		this.student = student;
	}

	public ActivityType getActivityType() {
		return activityType;
	}

	public void setActivityType(ActivityType activityType) {
		this.activityType = activityType;
	}

	public Integer getNumberOccurrenceGlosa() {
		return numberOccurrenceGlosa;
	}

	public void setNumberOccurrenceGlosa(Integer numberOccurrenceGlosa) {
		this.numberOccurrenceGlosa = numberOccurrenceGlosa;
	}

	public Integer getNumberHourGlosa() {
		return numberHourGlosa;
	}

	public void setNumberHourGlosa(Integer numberHourGlosa) {
		this.numberHourGlosa = numberHourGlosa;
	}

	public List<ActivityFile> getActivityFiles() {
		return activityFiles;
	}

	public void setActivityFiles(List<ActivityFile> activityFiles) {
		this.activityFiles = activityFiles;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
}
