package com.rrdevelopers.cac_nova_roma.control.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * @author Renatão
 *
 */
@Entity
@SequenceGenerator(initialValue = 1, name = "base_gen", sequenceName = "activity_history_seq")
@Table(name = "activity_history")
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_activity_history")) })
public class ActivityHistory extends GenericEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "tp_status_from", nullable = false, length = 50)
	private String statusFrom;

	@Column(name = "tp_status_to", nullable = false, length = 50)
	private String statusTo;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_activity", nullable = false)
	private Activity activity;

	public ActivityHistory() { }
	
	public ActivityHistory(String statusFrom, String statusTo, Activity activity) {
		this.statusFrom = statusFrom;
		this.statusTo = statusTo;
		this.activity = activity;
	}

	public String getStatusFrom() {
		return statusFrom;
	}

	public void setStatusFrom(String statusFrom) {
		this.statusFrom = statusFrom;
	}

	public String getStatusTo() {
		return statusTo;
	}

	public void setStatusTo(String statusTo) {
		this.statusTo = statusTo;
	}

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

}
