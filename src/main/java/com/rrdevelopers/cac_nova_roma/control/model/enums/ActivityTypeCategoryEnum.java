/**
 * 
 */
package com.rrdevelopers.cac_nova_roma.control.model.enums;

/**
 * @author rubens.ferreira
 *
 */
public enum ActivityTypeCategoryEnum implements IEnumDeserializer<ActivityTypeCategoryEnum>{
	
	HOUR("HOUR"),
	OCCURRENCE("OCCURRENCE");

	private final String text;

	private ActivityTypeCategoryEnum(String text) {
		this.text = text;
	}

	@Override
	public ActivityTypeCategoryEnum getTextEnumByString(String text) {
		for (ActivityTypeCategoryEnum activityTypeCategoryEnum : values())
			if (activityTypeCategoryEnum.text.equals(text))
				return activityTypeCategoryEnum;
		return null;
	}

	public String getText() {
		return text;
	}
}
