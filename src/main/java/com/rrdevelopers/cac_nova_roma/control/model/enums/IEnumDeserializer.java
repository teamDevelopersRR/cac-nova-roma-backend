package com.rrdevelopers.cac_nova_roma.control.model.enums;

public interface IEnumDeserializer<T> {
	
	T getTextEnumByString(String text);
	
}
